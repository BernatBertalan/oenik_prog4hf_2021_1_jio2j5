﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Program.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Positions.
    /// </summary>
    public enum PositionType
    {
        /// <summary>
        /// Right Winger attacker.
        /// </summary>
        RightWinger,

        /// <summary>
        /// Left Winger attacker
        /// </summary>
        LeftWinger,

        /// <summary>
        /// Centre Forward attacker.
        /// </summary>
        CentreForward,

        /// <summary>
        /// Central Midfield player.
        /// </summary>
        CentralMidfield,

        /// <summary>
        /// Centre Back defender
        /// </summary>
        CentreBack,

        /// <summary>
        /// Left Back defender.
        /// </summary>
        LeftBack,

        /// <summary>
        /// Left Midfield player.
        /// </summary>
        LeftMidfield,
    }

    /// <summary>
    /// Player models.
    /// </summary>
    public class Player : ObservableObject
    {
        private int playerid;
        private string name;
        private int age;
        private int goals;
        private string nation;
        private PositionType position;

        /// <summary>
        /// Gets or Sets ID of the player.
        /// </summary>
        public int PlayerId
        {
            get { return this.playerid; }
            set { this.Set(ref this.playerid, value); }
        }

        /// <summary>
        /// Gets or Sets Name of the player.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or Sets Age of the player.
        /// </summary>
        public int Age
        {
            get { return this.age; }
            set { this.Set(ref this.age, value); }
        }

        /// <summary>
        /// Gets or Sets number of Goals.
        /// </summary>
        public int Goals
        {
            get { return this.goals; }
            set { this.Set(ref this.goals, value); }
        }

        /// <summary>
        /// Gets or Sets Nation of the player.
        /// </summary>
        public string Nation
        {
            get { return this.nation; }
            set { this.Set(ref this.nation, value); }
        }

        /// <summary>
        /// Gets or Sets Position on the pitch.
        /// </summary>
        public PositionType Position
        {
            get { return this.position; }
            set { this.Set(ref this.position, value); }
        }

        /// <summary>
        /// CopyFrom method.
        /// </summary>
        /// <param name="other"> Other Player that we want to copy.</param>
        public void CopyFrom(Player other)
        {
            if (other != null)
            {
                this.Name = other.Name;
                this.Position = other.Position;
                this.Nation = other.Nation;
                this.Age = other.Age;
                this.Goals = other.Goals;
                this.PlayerId = other.PlayerId;
            }
        }
    }
}
