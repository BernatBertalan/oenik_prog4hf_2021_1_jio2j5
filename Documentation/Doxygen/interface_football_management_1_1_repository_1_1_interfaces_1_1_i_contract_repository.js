var interface_football_management_1_1_repository_1_1_interfaces_1_1_i_contract_repository =
[
    [ "AddContract", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_contract_repository.html#a00611e78a4e2723f4591dffd290e5018", null ],
    [ "ChangeAgent", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_contract_repository.html#ac823d149583fba7fd641aacd535a755f", null ],
    [ "ChangeContractDate", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_contract_repository.html#aaf8a09a6cf70f32c2c44287e055edef0", null ],
    [ "ChangeContractYears", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_contract_repository.html#ae24d240f568e04d1b819a2327ae0e386", null ],
    [ "ChangeReleaseClause", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_contract_repository.html#a62bef1f6be2bcd8ea5335e119a34c43b", null ],
    [ "ChangeSalary", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_contract_repository.html#a1a7b3fe89036a03a52fa2e770a2bcd47", null ],
    [ "Delete", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_contract_repository.html#adcd118e83c5320ef10d37f0d5fedbf18", null ],
    [ "GetOne", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_contract_repository.html#a5106bb1cf08ebdb49544371391aa4671", null ]
];