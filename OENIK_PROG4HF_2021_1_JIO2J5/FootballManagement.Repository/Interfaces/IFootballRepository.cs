﻿// <copyright file="IFootballRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballManagement.Data;

    /// <summary>
    /// IRepository ancient interface.
    /// </summary>
    /// <typeparam name="T">Type of the current Repository.</typeparam>
    public interface IFootballRepository<T>
        where T : class
    {
        /// <summary>
        /// Get All element of the tables.
        /// </summary>
        /// <returns>All elements of the table.</returns>
        IQueryable<T> GetAll();
    }
}
