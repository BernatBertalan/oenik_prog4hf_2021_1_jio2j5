﻿// <copyright file="EditorServiceViaWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Program.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballManagement.Program.BL;
    using FootballManagement.Program.Data;

    /// <summary>
    /// Implements IEditorService interface.
    /// </summary>
    public class EditorServiceViaWindow : IEditorService
    {
        /// <summary>
        /// Returns that editing is ok or cancel.
        /// </summary>
        /// <param name="p">An input Club.</param>
        /// <returns>True or false.</returns>
        public bool EditClub(Club p)
        {
            ClubEditorWindow win = new ClubEditorWindow(p);
            return win.ShowDialog() ?? false;
        }

        /// <summary>
        /// Returns that editing is ok or cancel.
        /// </summary>
        /// <param name="p">An input Contract.</param>
        /// <returns>True or false.</returns>
        public bool EditContract(Contract p)
        {
            ContractEditorWindow win = new ContractEditorWindow(p);
            return win.ShowDialog() ?? false;
        }

        /// <summary>
        /// Returns that editing is ok or cancel.
        /// </summary>
        /// <param name="p">An input Player.</param>
        /// <returns>True or false.</returns>
        public bool EditPlayer(Player p)
        {
            PlayerEditorWindow win = new PlayerEditorWindow(p);
            return win.ShowDialog() ?? false;
        }
    }
}
