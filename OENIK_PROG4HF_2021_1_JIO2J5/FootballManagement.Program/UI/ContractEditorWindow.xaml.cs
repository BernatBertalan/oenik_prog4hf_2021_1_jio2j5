﻿// <copyright file="ContractEditorWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Program.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using FootballManagement.Program.Data;
    using FootballManagement.Program.VM;

    /// <summary>
    /// Interaction logic for ContractEditorWindow.xaml.
    /// </summary>
    public partial class ContractEditorWindow : Window
    {
        private ContractEditorViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractEditorWindow"/> class.
        /// </summary>
        public ContractEditorWindow()
        {
            this.InitializeComponent();

            this.vM = this.FindResource("VM") as ContractEditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractEditorWindow"/> class.
        /// </summary>
        /// <param name="oldcontract">An input Contract.</param>
        public ContractEditorWindow(Contract oldcontract)
            : this()
        {
            this.vM.Contract = oldcontract;
        }

        private void OkClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
