﻿// <copyright file="IContractRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using FootballManagement.Data;

    /// <summary>
    /// IContractRepository.
    /// </summary>
    public interface IContractRepository : IFootballRepository<Contract>
    {
        /// <summary>
        /// Change number of Contract years.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newContractYears">New number of Contract years.</param>
        void ChangeContractYears(int playerid, int clubid, int newContractYears);

        /// <summary>
        /// Change date of contract.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newContractDate">New date of contract.</param>
        void ChangeContractDate(int playerid, int clubid, string newContractDate);

        /// <summary>
        /// Change the Salary of the player.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newSalary">New value of Salary.</param>
        void ChangeSalary(int playerid, int clubid, int newSalary);

        /// <summary>
        /// Change the Release clause.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newReleaseClause">New Release clause of the player.</param>
        void ChangeReleaseClause(int playerid, int clubid, int newReleaseClause);

        /// <summary>
        /// Change the player agent.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newAgent">New player agent.</param>
        void ChangeAgent(int playerid, int clubid, string newAgent);

        /// <summary>
        /// Delete a row.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        void Delete(int playerid, int clubid);

        /// <summary>
        /// Get Contract By Id.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <returns>Returns the contract with the given id's.</returns>
        Contract GetOne(int playerid, int clubid);

        /// <summary>
        /// Add a new Contract.
        /// </summary>
        /// <param name="playerid">Id of the player you wants to add.</param>
        /// <param name="clubid">Id of the clubs who signed the player.</param>
        /// <param name="contractyears">Contract years.</param>
        /// <param name="contractdate">Date of the contract.</param>
        /// <param name="salary">Salary in euro.</param>
        /// <param name="releaseclause">Release Clause in euro.</param>
        /// <param name="agent">Player Agent.</param>
        void AddContract(int playerid, int clubid, int contractyears, string contractdate, int salary, int releaseclause, string agent);
    }
}
