﻿// <copyright file="Club.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Reflection;
    using FootballManagement.Data.Model;

    /// <summary>
    /// Clubs Table.
    /// </summary>
    [Table("Clubs")]
    public class Club
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Club"/> class.
        /// </summary>
        public Club()
        {
            this.Contracts = new HashSet<Contract>();
        }

        /// <summary>
        /// Gets or Sets ClubId.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [ToString]
        public int ClubId { get; set; }

        /// <summary>
        /// Gets or Sets Name of the club.
        /// </summary>
        [Required]
        [MaxLength(100)]
        [ToString]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets Country of the club.
        /// </summary>
        [ToString]
        public string Country { get; set; }

        /// <summary>
        /// Gets or Sets Coach of the club.
        /// </summary>
        [ToString]
        public string Coach { get; set; }

        /// <summary>
        /// Gets or Sets when was founded.
        /// </summary>
        [ToString]
        public int Founded { get; set; }

        /// <summary>
        /// Gets or Sets number of Championship wins.
        /// </summary>
        [ToString]
        public int ChampionshipWins { get; set; }

        /// <summary>
        /// Gets MainData to write out.
        /// </summary>
        public string MainData => $"\n Club Name: {this.Name} \n Founded: {this.Founded} \n Coach: {this.Coach} \n Country: {this.Country} \n Championship Wins: {this.ChampionshipWins}";

        /// <summary>
        /// Gets Contracts collection.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Contract> Contracts { get; }

        /// <summary>
        /// Overrided to String.
        /// </summary>
        /// <returns>Only the selected elements in the table.</returns>
        public override string ToString()
        {
            string x = string.Empty;

            foreach (var item in this.GetType().GetProperties().Where(x => x.GetCustomAttribute<ToStringAttribute>() != null))
            {
                x += "  ";
                x += item.Name + ":  ";
                x += item.GetValue(this);
                x += "\n";
            }

            return x;
        }

        /// <summary>
        /// Overrided equals method.
        /// </summary>
        /// <param name="obj">An object. </param>
        /// <returns>Return true or false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Club)
            {
                Club other = obj as Club;
                return this.Name == other.Name && this.ClubId == other.ClubId && this.ChampionshipWins == other.ChampionshipWins && this.Coach == other.Coach && this.Country == other.Country && this.Founded == other.Founded;
            }

            return false;
        }

        /// <summary>
        /// Overrided GetHashCode.
        /// </summary>
        /// <returns>Obect hashcode.</returns>
        public override int GetHashCode()
        {
            return (int)this.ClubId;
        }
    }
}
