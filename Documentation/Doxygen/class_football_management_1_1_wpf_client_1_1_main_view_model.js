var class_football_management_1_1_wpf_client_1_1_main_view_model =
[
    [ "MainViewModel", "class_football_management_1_1_wpf_client_1_1_main_view_model.html#a027d5d4a65180d175830500dde2a2878", null ],
    [ "MainViewModel", "class_football_management_1_1_wpf_client_1_1_main_view_model.html#a8538210578433db928325fdfd303f606", null ],
    [ "AddCmd", "class_football_management_1_1_wpf_client_1_1_main_view_model.html#a34cf9b05d70f837429809f167390f671", null ],
    [ "AllPlayers", "class_football_management_1_1_wpf_client_1_1_main_view_model.html#aa89b188ed538f7a4345afe79ebbf0b5a", null ],
    [ "DelCmd", "class_football_management_1_1_wpf_client_1_1_main_view_model.html#a79db90e8f2320b6c19c9b2047f916102", null ],
    [ "EditorFunc", "class_football_management_1_1_wpf_client_1_1_main_view_model.html#aa4716013cfc95433cd2381011cea86c4", null ],
    [ "LoadCmd", "class_football_management_1_1_wpf_client_1_1_main_view_model.html#aa3809d99ad64713cc76e2a8e55030a8d", null ],
    [ "ModCmd", "class_football_management_1_1_wpf_client_1_1_main_view_model.html#a11b9b744627d625853841e0fc4f57317", null ],
    [ "SelectedPlayer", "class_football_management_1_1_wpf_client_1_1_main_view_model.html#a101534fb7ca6466f30331ed0e3ae5f3c", null ]
];