﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Data
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Reflection;
    using FootballManagement.Data.Model;

    /// <summary>
    /// Players Table.
    /// </summary>
    [Table("Players")]
    public class Player
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        public Player()
        {
            this.Contracts = new HashSet<Contract>();
        }

        /// <summary>
        /// Gets or Sets PlayerId.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [ToString]
        public int PlayerId { get; set; }

        /// <summary>
        /// Gets or Sets Name of the player.
        /// </summary>
        [Required]
        [MaxLength(100)]
        [ToString]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets Age of the player.
        /// </summary>
        [ToString]
        public int Age { get; set; }

        /// <summary>
        /// Gets or Sets number of Goals.
        /// </summary>
        [ToString]
        public int Goals { get; set; }

        /// <summary>
        /// Gets or Sets Nation of the player.
        /// </summary>
        [ToString]
        public string Nation { get; set; }

        /// <summary>
        /// Gets or Sets Position on the pitch.
        /// </summary>
        [ToString]
        public string Position { get; set; }

        /// <summary>
        /// Gets MainData to write out.
        /// </summary>
        public string MainData => $"\n Full Name: {this.Name} \n Age: {this.Age} \n Goals: {this.Goals} \n Nation: {this.Nation} \n Position: {this.Position}";

        /// <summary>
        /// Gets Clubs.
        /// </summary>
        [NotMapped]
        public virtual ICollection<Contract> Contracts { get; }

        /// <summary>
        /// Overrided to String.
        /// </summary>
        /// <returns>Only the selected elements in the table.</returns>
        public override string ToString()
        {
            string x = string.Empty;

            foreach (var item in this.GetType().GetProperties().Where(x => x.GetCustomAttribute<ToStringAttribute>() != null))
            {
                x += "  ";
                x += item.Name + ":  ";
                x += item.GetValue(this);
                x += "\n";
            }

            return x;
        }

        /// <summary>
        /// Overrided equals method.
        /// </summary>
        /// <param name="obj">An object. </param>
        /// <returns>Return true or false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Player)
            {
                Player other = obj as Player;
                return this.Age == other.Age && this.Name == other.Name && this.PlayerId == other.PlayerId && this.Goals == other.Goals && this.Position == other.Position && this.Nation == other.Nation;
            }

            return false;
        }

        /// <summary>
        /// Overrided GetHashCode.
        /// </summary>
        /// <returns>Obect hashcode.</returns>
        public override int GetHashCode()
        {
            return (int)this.PlayerId;
        }
    }
}
