var namespace_football_management_1_1_logic_1_1_return_classes =
[
    [ "ClubGoals", "class_football_management_1_1_logic_1_1_return_classes_1_1_club_goals.html", "class_football_management_1_1_logic_1_1_return_classes_1_1_club_goals" ],
    [ "MostWins", "class_football_management_1_1_logic_1_1_return_classes_1_1_most_wins.html", "class_football_management_1_1_logic_1_1_return_classes_1_1_most_wins" ],
    [ "PlayerWithContract", "class_football_management_1_1_logic_1_1_return_classes_1_1_player_with_contract.html", "class_football_management_1_1_logic_1_1_return_classes_1_1_player_with_contract" ],
    [ "Salaries", "class_football_management_1_1_logic_1_1_return_classes_1_1_salaries.html", "class_football_management_1_1_logic_1_1_return_classes_1_1_salaries" ]
];