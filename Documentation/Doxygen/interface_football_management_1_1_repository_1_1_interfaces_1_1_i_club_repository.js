var interface_football_management_1_1_repository_1_1_interfaces_1_1_i_club_repository =
[
    [ "AddClub", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_club_repository.html#a6396615f188413894e3a37c7474568ab", null ],
    [ "ChangeChampionshipWins", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_club_repository.html#ac4ba474cd3f2915c982c46000e8d0926", null ],
    [ "ChangeCoach", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_club_repository.html#a5230e55a6bf19a13197115093a0a20ff", null ],
    [ "ChangeCountry", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_club_repository.html#a2a8e2bc23e6f30479ee5e8be3d2b280d", null ],
    [ "ChangeFounded", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_club_repository.html#a50c66a91e7a9fe021c6b43f7996c4e92", null ],
    [ "ChangeName", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_club_repository.html#a4dd36e8efed10784b0afee68f2d8bb75", null ],
    [ "Delete", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_club_repository.html#aa9230b65c77bad9a4570fd410c5eb8ed", null ],
    [ "GetOne", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_club_repository.html#a8b838cd5f331e11eef9fa82087050078", null ]
];