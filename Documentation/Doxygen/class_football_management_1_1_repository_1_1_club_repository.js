var class_football_management_1_1_repository_1_1_club_repository =
[
    [ "ClubRepository", "class_football_management_1_1_repository_1_1_club_repository.html#abc6a9ee595f0d69235d94e711ed49b50", null ],
    [ "AddClub", "class_football_management_1_1_repository_1_1_club_repository.html#a2798dd4a3b6f82b3344d57ea1e090e9d", null ],
    [ "ChangeChampionshipWins", "class_football_management_1_1_repository_1_1_club_repository.html#accceba6930830809353eb5d17df2c102", null ],
    [ "ChangeCoach", "class_football_management_1_1_repository_1_1_club_repository.html#a9d96d34988f32090ad2d57c87f118469", null ],
    [ "ChangeCountry", "class_football_management_1_1_repository_1_1_club_repository.html#a81b4f492b74b621ce3cda847125b09d0", null ],
    [ "ChangeFounded", "class_football_management_1_1_repository_1_1_club_repository.html#a548ff9849a66ba0f1050c233bddbc76a", null ],
    [ "ChangeName", "class_football_management_1_1_repository_1_1_club_repository.html#a55a171c521ca3921411af3d65766bcdb", null ],
    [ "Delete", "class_football_management_1_1_repository_1_1_club_repository.html#a7e6a2bdc008976d436d19501151d8bc9", null ],
    [ "GetOne", "class_football_management_1_1_repository_1_1_club_repository.html#a70d4658359e68ca572cfd8bb15dbeed0", null ]
];