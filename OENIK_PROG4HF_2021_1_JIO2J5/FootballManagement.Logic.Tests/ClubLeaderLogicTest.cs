﻿// <copyright file="ClubLeaderLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballManagement.Data;
    using FootballManagement.Logic.ReturnClasses;
    using FootballManagement.Repository.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test ClubLeaderLogicTest cruds and non cruds.
    /// </summary>
    [TestFixture]
    public class ClubLeaderLogicTest
    {
        private static List<Player> players = new List<Player>()
        {
                new Player() { PlayerId = 1, Name = "Lionel Messi", Age = 33, Goals = 706, Nation = "Argentina", Position = "Right Winger" },
                new Player() { PlayerId = 2, Name = "Cristiano Ronaldo", Age = 35, Goals = 742, Nation = "Portugal", Position = "Left Winger" },
                new Player() { PlayerId = 3, Name = "Szoboszlai Dominik", Age = 19, Goals = 39, Nation = "Hungary", Position = "Left Midfield" },
                new Player() { PlayerId = 4, Name = "Luis Suárez", Age = 33, Goals = 467, Nation = "Uruguay", Position = "Centre-Forward" },
                new Player() { PlayerId = 5, Name = "Robert Lewandowski", Age = 32, Goals = 477, Nation = "Poland", Position = "Centre-Forward" },
                new Player() { PlayerId = 6, Name = "Andrés Iniesta", Age = 36, Goals = 84, Nation = "Spain", Position = "Central Midfield" },
                new Player() { PlayerId = 7, Name = "Marcelo", Age = 32, Goals = 47, Nation = "Brazil", Position = "Left-Back" },
                new Player() { PlayerId = 8, Name = "Virgil van Dijk", Age = 29, Goals = 47, Nation = "Netherlands", Position = "Centre-Back" },
                new Player() { PlayerId = 9, Name = "Neymar Junior", Age = 28, Goals = 311, Nation = "Brazil", Position = "Left Winger" },
                new Player() { PlayerId = 10, Name = "Zlatan Ibrahimović", Age = 39, Goals = 532, Nation = "Sweeden", Position = "Centre-Forward" },
                new Player() { PlayerId = 11, Name = "David Neres", Age = 23, Goals = 39, Nation = "Brazil", Position = "Right Winger" },
        };

        private static List<Club> clubs = new List<Club>()
        {
            new Club() { ClubId = 1, Name = "FC Barcelona", Coach = "Ronald Koeman", Founded = 1899, Country = "Spain", ChampionshipWins = 26 },
            new Club() { ClubId = 2, Name = "Juventus FC", Coach = "Andrea Pirlo", Founded = 1897, Country = "Italy", ChampionshipWins = 36 },
            new Club() { ClubId = 3, Name = "Red Bull Salzburg", Coach = "Jesse Marsch", Founded = 1933, Country = "Spain", ChampionshipWins = 14 },
            new Club() { ClubId = 4, Name = "Atlético Madrid", Coach = "Diego Simeone", Founded = 1903, Country = "Spain", ChampionshipWins = 10 },
            new Club() { ClubId = 5, Name = "Bayern Münich", Coach = "Hans-Dieter Flick", Founded = 1900, Country = "Germany", ChampionshipWins = 30 },
            new Club() { ClubId = 6, Name = "Vissel Kobe", Coach = "Atsuhiro Miura", Founded = 1965, Country = "Japan", ChampionshipWins = 5 },
            new Club() { ClubId = 7, Name = "Real Madrid", Coach = "Zinédine Zidane", Founded = 1902, Country = "Spain", ChampionshipWins = 34 },
            new Club() { ClubId = 8, Name = "Liverpool FC", Coach = "Jürgen Klopp", Founded = 1892, Country = "England", ChampionshipWins = 19 },
            new Club() { ClubId = 9, Name = "Paris Saint-Germain", Coach = "Thomas Tuchel", Founded = 1970, Country = "France", ChampionshipWins = 9 },
            new Club() { ClubId = 10, Name = "AC Milan", Coach = "Stefano Pioli", Founded = 1899, Country = "Italy", ChampionshipWins = 18 },
            new Club() { ClubId = 11, Name = "Ajax Amsterdam", Coach = "Erik ten Hag", Founded = 1900, Country = "Netherlands", ChampionshipWins = 34 },
        };

        private static List<Contract> contracts = new List<Contract>()
        {
                new Contract() { PlayerId = 1,  ClubId = 1, ContractYears = 1, ContractDate = "2018.09.30", Salary = 45000000, ReleaseClause = 700000000, Agent = "Jorge Mendes" },
                new Contract() { PlayerId = 2, ClubId = 2, ContractYears = 3, ContractDate = "2017.11.13", Salary = 40000000, ReleaseClause = 500000000, Agent = "Jorge Mendes" },
                new Contract() { PlayerId = 3, ClubId = 3, ContractYears = 3, ContractDate = "2019.09.25", Salary = 2000000, ReleaseClause = 20000000, Agent = "Mino Raiola" },
                new Contract() { PlayerId = 4, ClubId = 4, ContractYears = 1, ContractDate = "2020.09.31", Salary = 23200000, ReleaseClause = 200000000, Agent = "Jonathan Barnett" },
                new Contract() { PlayerId = 5, ClubId = 5, ContractYears = 4, ContractDate = "2019.11.24", Salary = 27600000, ReleaseClause = 400000000, Agent = "Juan Figer" },
                new Contract() { PlayerId = 6, ClubId = 6, ContractYears = 1, ContractDate = "2018.09.30", Salary = 25000000, ReleaseClause = 100000000, Agent = "Pep Guardiola" },
                new Contract() { PlayerId = 7, ClubId = 7, ContractYears = 1, ContractDate = "2018.09.30", Salary = 6750000, ReleaseClause = 80000000, Agent = "Juan Figer" },
                new Contract() { PlayerId = 8, ClubId = 8, ContractYears = 1, ContractDate = "2018.09.30", Salary = 11460000, ReleaseClause = 140000000, Agent = "Jonathan Barnett" },
                new Contract() { PlayerId = 9, ClubId = 9, ContractYears = 1, ContractDate = "2018.09.30", Salary = 3580000, ReleaseClause = 400000000, Agent = "Jorge Mendes" },
                new Contract() { PlayerId = 10, ClubId = 10, ContractYears = 1, ContractDate = "2018.09.30", Salary = 45000000, ReleaseClause = 150000000, Agent = "Mino Raiola" },
                new Contract() { PlayerId = 11, ClubId = 11, ContractYears = 1, ContractDate = "2018.09.30", Salary = 45000000, ReleaseClause = 38000000, Agent = "Jorge Mendes" },
        };

        /// <summary>
        /// Tests goals/clubs noncrud method.
        /// </summary>
        [Test]
        public void TestGoalsPerClubs()
        {
            Mock<IPlayerRepository> mprepo = new Mock<IPlayerRepository>();
            Mock<IContractRepository> mcrepo = new Mock<IContractRepository>();
            Mock<IClubRepository> mkrepo = new Mock<IClubRepository>();

            List<ClubGoals> expected = new List<ClubGoals>()
            {
                new ClubGoals() { ClubName = "Juventus FC", Goals = 742 },
                new ClubGoals() { ClubName = "FC Barcelona", Goals = 706 },
                new ClubGoals() { ClubName = "AC Milan", Goals = 532 },
                new ClubGoals() { ClubName = "Bayern Münich", Goals = 477 },
                new ClubGoals() { ClubName = "Atlético Madrid", Goals = 467 },
                new ClubGoals() { ClubName = "Paris Saint-Germain", Goals = 311 },
                new ClubGoals() { ClubName = "Vissel Kobe", Goals = 84 },
                new ClubGoals() { ClubName = "Real Madrid", Goals = 47 },
                new ClubGoals() { ClubName = "Liverpool FC", Goals = 47 },
                new ClubGoals() { ClubName = "Red Bull Salzburg", Goals = 39 },
                new ClubGoals() { ClubName = "Ajax Amsterdam", Goals = 39 },
            };

            mprepo.Setup(x => x.GetAll()).Returns(players.AsQueryable());
            mcrepo.Setup(x => x.GetAll()).Returns(contracts.AsQueryable());
            mkrepo.Setup(x => x.GetAll()).Returns(clubs.AsQueryable());

            ClubLeaderLogic logic = new ClubLeaderLogic(mkrepo.Object, mcrepo.Object, mprepo.Object);
            var clubgoals = logic.GoalsPerClubs();

            Assert.That(clubgoals, Is.EquivalentTo(expected));
            Assert.That(clubgoals.Count, Is.EqualTo(expected.Count));

            mprepo.Verify(x => x.GetAll(), Times.Once);
            mcrepo.Verify(x => x.GetAll(), Times.Once);
            mkrepo.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests most championship wins noncrud method.
        /// </summary>
        [Test]
        public void TestMostChampionshipWins()
        {
            Mock<IPlayerRepository> mprepo = new Mock<IPlayerRepository>();
            Mock<IContractRepository> mcrepo = new Mock<IContractRepository>();
            Mock<IClubRepository> mkrepo = new Mock<IClubRepository>();

            MostWins expected = new MostWins() { ClubName = "Juventus FC", ChampionshipWins = 36 };

            mkrepo.Setup(x => x.GetAll()).Returns(clubs.AsQueryable());

            ClubLeaderLogic logic = new ClubLeaderLogic(mkrepo.Object, mcrepo.Object, mprepo.Object);
            var mostwins = logic.MostChampionshipWins()[0];

            Assert.That(mostwins, Is.EqualTo(expected));

            mkrepo.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests Clubs get all crud method.
        /// </summary>
        [Test]
        public void TestGetAllClubs()
        {
            Mock<IPlayerRepository> mprepo = new Mock<IPlayerRepository>();
            Mock<IContractRepository> mcrepo = new Mock<IContractRepository>();
            Mock<IClubRepository> mkrepo = new Mock<IClubRepository>();

            List<Club> expected = new List<Club>()
            {
            new Club() { ClubId = 1, Name = "FC Barcelona", Coach = "Ronald Koeman", Founded = 1899, Country = "Spain", ChampionshipWins = 26 },
            new Club() { ClubId = 2, Name = "Juventus FC", Coach = "Andrea Pirlo", Founded = 1897, Country = "Italy", ChampionshipWins = 36 },
            new Club() { ClubId = 3, Name = "Red Bull Salzburg", Coach = "Jesse Marsch", Founded = 1933, Country = "Spain", ChampionshipWins = 14 },
            new Club() { ClubId = 4, Name = "Atlético Madrid", Coach = "Diego Simeone", Founded = 1903, Country = "Spain", ChampionshipWins = 10 },
            new Club() { ClubId = 5, Name = "Bayern Münich", Coach = "Hans-Dieter Flick", Founded = 1900, Country = "Germany", ChampionshipWins = 30 },
            new Club() { ClubId = 6, Name = "Vissel Kobe", Coach = "Atsuhiro Miura", Founded = 1965, Country = "Japan", ChampionshipWins = 5 },
            new Club() { ClubId = 7, Name = "Real Madrid", Coach = "Zinédine Zidane", Founded = 1902, Country = "Spain", ChampionshipWins = 34 },
            new Club() { ClubId = 8, Name = "Liverpool FC", Coach = "Jürgen Klopp", Founded = 1892, Country = "England", ChampionshipWins = 19 },
            new Club() { ClubId = 9, Name = "Paris Saint-Germain", Coach = "Thomas Tuchel", Founded = 1970, Country = "France", ChampionshipWins = 9 },
            new Club() { ClubId = 10, Name = "AC Milan", Coach = "Stefano Pioli", Founded = 1899, Country = "Italy", ChampionshipWins = 18 },
            new Club() { ClubId = 11, Name = "Ajax Amsterdam", Coach = "Erik ten Hag", Founded = 1900, Country = "Netherlands", ChampionshipWins = 34 },
            };

            mkrepo.Setup(x => x.GetAll()).Returns(clubs.AsQueryable());
            ClubLeaderLogic logic = new ClubLeaderLogic(mkrepo.Object, mcrepo.Object, mprepo.Object);

            var allclubs = logic.GetAllClubs();

            Assert.That(allclubs.Count, Is.EqualTo(expected.Count));
            Assert.That(allclubs, Is.EquivalentTo(expected));

            mkrepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests clubs change name crud method.
        /// </summary>
        [Test]
        public void TestChangeName()
        {
            Mock<IPlayerRepository> mprepo = new Mock<IPlayerRepository>();
            Mock<IContractRepository> mcrepo = new Mock<IContractRepository>();
            Mock<IClubRepository> mkrepo = new Mock<IClubRepository>();

            mkrepo.Setup(x => x.ChangeName(1, "Barcelona FC"));
            ClubLeaderLogic logic = new ClubLeaderLogic(mkrepo.Object, mcrepo.Object, mprepo.Object);

            logic.ChangeName(1, "Barcelona FC");
            mkrepo.Verify(x => x.ChangeName(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }
    }
}
