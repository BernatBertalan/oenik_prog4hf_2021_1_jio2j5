var interface_football_management_1_1_repository_1_1_interfaces_1_1_i_player_repository =
[
    [ "AddPlayer", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_player_repository.html#a6a7d73ba3a3eeb16d92e5d4b73ebc5f5", null ],
    [ "ChangeAge", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_player_repository.html#ac015fb56c2a311a82da2e0f87a612d3a", null ],
    [ "ChangeGoals", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_player_repository.html#ac8c6252a59507bd798a03165ab68a245", null ],
    [ "ChangeName", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_player_repository.html#adf456c992d95cedbeee743e6fab8234b", null ],
    [ "ChangeNation", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_player_repository.html#ad554782cc28574395dce22eb58f3c5a7", null ],
    [ "ChangePosition", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_player_repository.html#a8e6541377271bd070fe71f4f33d9b65d", null ],
    [ "Delete", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_player_repository.html#a93bb9d92c738eac148cf82b724c17af0", null ],
    [ "GetOne", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_player_repository.html#a95bbb5b6249ffabab58f080e6ce42b2a", null ]
];