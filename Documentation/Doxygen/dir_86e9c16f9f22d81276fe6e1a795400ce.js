var dir_86e9c16f9f22d81276fe6e1a795400ce =
[
    [ "Football.Web", "dir_f9e3a09011d867c10b21be4ecf7e556b.html", "dir_f9e3a09011d867c10b21be4ecf7e556b" ],
    [ "FootballManagement.Data", "dir_4a32bc83c47a38f5ba48f0f60e9ac0e7.html", "dir_4a32bc83c47a38f5ba48f0f60e9ac0e7" ],
    [ "FootballManagement.Logic", "dir_066ff03c40bb82d7beaa778c59787fe9.html", "dir_066ff03c40bb82d7beaa778c59787fe9" ],
    [ "FootballManagement.Logic.Tests", "dir_3531c09ff1b35d30ff2cfe891dbfc47e.html", "dir_3531c09ff1b35d30ff2cfe891dbfc47e" ],
    [ "FootballManagement.Program", "dir_f43b589c6b61cd3dc1467223ed5d7708.html", "dir_f43b589c6b61cd3dc1467223ed5d7708" ],
    [ "FootballManagement.Repository", "dir_44591347d45d9e1af7819ba4b941bbb3.html", "dir_44591347d45d9e1af7819ba4b941bbb3" ],
    [ "FootballManagement.WpfClient", "dir_4a495519a8a1896a7959791ba5515edc.html", "dir_4a495519a8a1896a7959791ba5515edc" ]
];