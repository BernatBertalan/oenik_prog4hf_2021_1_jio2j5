var annotated_dup =
[
    [ "AspNetCore", "namespace_asp_net_core.html", [
      [ "Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", "class_asp_net_core_1_1_views_____view_imports" ],
      [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
      [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
      [ "Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", "class_asp_net_core_1_1_views___home___privacy" ],
      [ "Views_Players_PlayersDetails", "class_asp_net_core_1_1_views___players___players_details.html", "class_asp_net_core_1_1_views___players___players_details" ],
      [ "Views_Players_PlayersEdit", "class_asp_net_core_1_1_views___players___players_edit.html", "class_asp_net_core_1_1_views___players___players_edit" ],
      [ "Views_Players_PlayersIndex", "class_asp_net_core_1_1_views___players___players_index.html", "class_asp_net_core_1_1_views___players___players_index" ],
      [ "Views_Players_PlayersList", "class_asp_net_core_1_1_views___players___players_list.html", "class_asp_net_core_1_1_views___players___players_list" ],
      [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
      [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ],
      [ "Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", "class_asp_net_core_1_1_views___shared___error" ]
    ] ],
    [ "FootballManagement", "namespace_football_management.html", [
      [ "Data", "namespace_football_management_1_1_data.html", [
        [ "Model", "namespace_football_management_1_1_data_1_1_model.html", [
          [ "ToStringAttribute", "class_football_management_1_1_data_1_1_model_1_1_to_string_attribute.html", null ]
        ] ],
        [ "Club", "class_football_management_1_1_data_1_1_club.html", "class_football_management_1_1_data_1_1_club" ],
        [ "Contract", "class_football_management_1_1_data_1_1_contract.html", "class_football_management_1_1_data_1_1_contract" ],
        [ "FootballContext", "class_football_management_1_1_data_1_1_football_context.html", "class_football_management_1_1_data_1_1_football_context" ],
        [ "Player", "class_football_management_1_1_data_1_1_player.html", "class_football_management_1_1_data_1_1_player" ]
      ] ],
      [ "Logic", "namespace_football_management_1_1_logic.html", [
        [ "ReturnClasses", "namespace_football_management_1_1_logic_1_1_return_classes.html", [
          [ "ClubGoals", "class_football_management_1_1_logic_1_1_return_classes_1_1_club_goals.html", "class_football_management_1_1_logic_1_1_return_classes_1_1_club_goals" ],
          [ "MostWins", "class_football_management_1_1_logic_1_1_return_classes_1_1_most_wins.html", "class_football_management_1_1_logic_1_1_return_classes_1_1_most_wins" ],
          [ "PlayerWithContract", "class_football_management_1_1_logic_1_1_return_classes_1_1_player_with_contract.html", "class_football_management_1_1_logic_1_1_return_classes_1_1_player_with_contract" ],
          [ "Salaries", "class_football_management_1_1_logic_1_1_return_classes_1_1_salaries.html", "class_football_management_1_1_logic_1_1_return_classes_1_1_salaries" ]
        ] ],
        [ "Tests", "namespace_football_management_1_1_logic_1_1_tests.html", [
          [ "ClubLeaderLogicTest", "class_football_management_1_1_logic_1_1_tests_1_1_club_leader_logic_test.html", "class_football_management_1_1_logic_1_1_tests_1_1_club_leader_logic_test" ],
          [ "ContractManagementLogicTest", "class_football_management_1_1_logic_1_1_tests_1_1_contract_management_logic_test.html", "class_football_management_1_1_logic_1_1_tests_1_1_contract_management_logic_test" ]
        ] ],
        [ "ClubLeaderLogic", "class_football_management_1_1_logic_1_1_club_leader_logic.html", "class_football_management_1_1_logic_1_1_club_leader_logic" ],
        [ "ContractManagementLogic", "class_football_management_1_1_logic_1_1_contract_management_logic.html", "class_football_management_1_1_logic_1_1_contract_management_logic" ],
        [ "IClubLeaderLogic", "interface_football_management_1_1_logic_1_1_i_club_leader_logic.html", "interface_football_management_1_1_logic_1_1_i_club_leader_logic" ],
        [ "IContractManagementLogic", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html", "interface_football_management_1_1_logic_1_1_i_contract_management_logic" ]
      ] ],
      [ "Program", "namespace_football_management_1_1_program.html", [
        [ "BL", "namespace_football_management_1_1_program_1_1_b_l.html", [
          [ "FootballLogic", "class_football_management_1_1_program_1_1_b_l_1_1_football_logic.html", "class_football_management_1_1_program_1_1_b_l_1_1_football_logic" ],
          [ "IEditorService", "interface_football_management_1_1_program_1_1_b_l_1_1_i_editor_service.html", "interface_football_management_1_1_program_1_1_b_l_1_1_i_editor_service" ],
          [ "IFootballLogic", "interface_football_management_1_1_program_1_1_b_l_1_1_i_football_logic.html", "interface_football_management_1_1_program_1_1_b_l_1_1_i_football_logic" ]
        ] ],
        [ "Data", "namespace_football_management_1_1_program_1_1_data.html", [
          [ "Club", "class_football_management_1_1_program_1_1_data_1_1_club.html", "class_football_management_1_1_program_1_1_data_1_1_club" ],
          [ "Contract", "class_football_management_1_1_program_1_1_data_1_1_contract.html", "class_football_management_1_1_program_1_1_data_1_1_contract" ],
          [ "Player", "class_football_management_1_1_program_1_1_data_1_1_player.html", "class_football_management_1_1_program_1_1_data_1_1_player" ]
        ] ],
        [ "UI", "namespace_football_management_1_1_program_1_1_u_i.html", [
          [ "ClubEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_club_editor_window.html", "class_football_management_1_1_program_1_1_u_i_1_1_club_editor_window" ],
          [ "ContractEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_contract_editor_window.html", "class_football_management_1_1_program_1_1_u_i_1_1_contract_editor_window" ],
          [ "EditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_editor_window.html", "class_football_management_1_1_program_1_1_u_i_1_1_editor_window" ],
          [ "PlayerEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_player_editor_window.html", "class_football_management_1_1_program_1_1_u_i_1_1_player_editor_window" ],
          [ "EditorServiceViaWindow", "class_football_management_1_1_program_1_1_u_i_1_1_editor_service_via_window.html", "class_football_management_1_1_program_1_1_u_i_1_1_editor_service_via_window" ]
        ] ],
        [ "VM", "namespace_football_management_1_1_program_1_1_v_m.html", [
          [ "ClubEditorViewModel", "class_football_management_1_1_program_1_1_v_m_1_1_club_editor_view_model.html", "class_football_management_1_1_program_1_1_v_m_1_1_club_editor_view_model" ],
          [ "ContractEditorViewModel", "class_football_management_1_1_program_1_1_v_m_1_1_contract_editor_view_model.html", "class_football_management_1_1_program_1_1_v_m_1_1_contract_editor_view_model" ],
          [ "MainViewModel", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model" ],
          [ "PlayerEditorViewModel", "class_football_management_1_1_program_1_1_v_m_1_1_player_editor_view_model.html", "class_football_management_1_1_program_1_1_v_m_1_1_player_editor_view_model" ]
        ] ],
        [ "App", "class_football_management_1_1_program_1_1_app.html", "class_football_management_1_1_program_1_1_app" ],
        [ "MainWindow", "class_football_management_1_1_program_1_1_main_window.html", "class_football_management_1_1_program_1_1_main_window" ],
        [ "MyIoc", "class_football_management_1_1_program_1_1_my_ioc.html", "class_football_management_1_1_program_1_1_my_ioc" ]
      ] ],
      [ "Repository", "namespace_football_management_1_1_repository.html", [
        [ "Interfaces", "namespace_football_management_1_1_repository_1_1_interfaces.html", [
          [ "IClubRepository", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_club_repository.html", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_club_repository" ],
          [ "IContractRepository", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_contract_repository.html", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_contract_repository" ],
          [ "IPlayerRepository", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_player_repository.html", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_player_repository" ]
        ] ],
        [ "ClubRepository", "class_football_management_1_1_repository_1_1_club_repository.html", "class_football_management_1_1_repository_1_1_club_repository" ],
        [ "ContractRepository", "class_football_management_1_1_repository_1_1_contract_repository.html", "class_football_management_1_1_repository_1_1_contract_repository" ],
        [ "FootballRepository", "class_football_management_1_1_repository_1_1_football_repository.html", "class_football_management_1_1_repository_1_1_football_repository" ],
        [ "PlayerRepository", "class_football_management_1_1_repository_1_1_player_repository.html", "class_football_management_1_1_repository_1_1_player_repository" ],
        [ "IFootballRepository", "interface_football_management_1_1_repository_1_1_i_football_repository.html", "interface_football_management_1_1_repository_1_1_i_football_repository" ]
      ] ],
      [ "Web", "namespace_football_management_1_1_web.html", [
        [ "Controllers", "namespace_football_management_1_1_web_1_1_controllers.html", [
          [ "HomeController", "class_football_management_1_1_web_1_1_controllers_1_1_home_controller.html", "class_football_management_1_1_web_1_1_controllers_1_1_home_controller" ],
          [ "PlayersApiController", "class_football_management_1_1_web_1_1_controllers_1_1_players_api_controller.html", "class_football_management_1_1_web_1_1_controllers_1_1_players_api_controller" ],
          [ "PlayersController", "class_football_management_1_1_web_1_1_controllers_1_1_players_controller.html", "class_football_management_1_1_web_1_1_controllers_1_1_players_controller" ]
        ] ],
        [ "Models", "namespace_football_management_1_1_web_1_1_models.html", [
          [ "ErrorViewModel", "class_football_management_1_1_web_1_1_models_1_1_error_view_model.html", "class_football_management_1_1_web_1_1_models_1_1_error_view_model" ],
          [ "MapperFactory", "class_football_management_1_1_web_1_1_models_1_1_mapper_factory.html", "class_football_management_1_1_web_1_1_models_1_1_mapper_factory" ],
          [ "Player", "class_football_management_1_1_web_1_1_models_1_1_player.html", "class_football_management_1_1_web_1_1_models_1_1_player" ],
          [ "PlayerListViewModel", "class_football_management_1_1_web_1_1_models_1_1_player_list_view_model.html", "class_football_management_1_1_web_1_1_models_1_1_player_list_view_model" ]
        ] ],
        [ "Program", "class_football_management_1_1_web_1_1_program.html", "class_football_management_1_1_web_1_1_program" ],
        [ "Startup", "class_football_management_1_1_web_1_1_startup.html", "class_football_management_1_1_web_1_1_startup" ]
      ] ],
      [ "WpfClient", "namespace_football_management_1_1_wpf_client.html", [
        [ "App", "class_football_management_1_1_wpf_client_1_1_app.html", "class_football_management_1_1_wpf_client_1_1_app" ],
        [ "EditorWindow", "class_football_management_1_1_wpf_client_1_1_editor_window.html", "class_football_management_1_1_wpf_client_1_1_editor_window" ],
        [ "IMainLogic", "interface_football_management_1_1_wpf_client_1_1_i_main_logic.html", "interface_football_management_1_1_wpf_client_1_1_i_main_logic" ],
        [ "MainLogic", "class_football_management_1_1_wpf_client_1_1_main_logic.html", "class_football_management_1_1_wpf_client_1_1_main_logic" ],
        [ "MainViewModel", "class_football_management_1_1_wpf_client_1_1_main_view_model.html", "class_football_management_1_1_wpf_client_1_1_main_view_model" ],
        [ "MainWindow", "class_football_management_1_1_wpf_client_1_1_main_window.html", "class_football_management_1_1_wpf_client_1_1_main_window" ],
        [ "PlayerVM", "class_football_management_1_1_wpf_client_1_1_player_v_m.html", "class_football_management_1_1_wpf_client_1_1_player_v_m" ]
      ] ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];