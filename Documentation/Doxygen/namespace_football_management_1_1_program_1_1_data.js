var namespace_football_management_1_1_program_1_1_data =
[
    [ "Club", "class_football_management_1_1_program_1_1_data_1_1_club.html", "class_football_management_1_1_program_1_1_data_1_1_club" ],
    [ "Contract", "class_football_management_1_1_program_1_1_data_1_1_contract.html", "class_football_management_1_1_program_1_1_data_1_1_contract" ],
    [ "Player", "class_football_management_1_1_program_1_1_data_1_1_player.html", "class_football_management_1_1_program_1_1_data_1_1_player" ],
    [ "PositionType", "namespace_football_management_1_1_program_1_1_data.html#af0ee946918ec0d1119ab67b27df4bd7a", [
      [ "RightWinger", "namespace_football_management_1_1_program_1_1_data.html#af0ee946918ec0d1119ab67b27df4bd7aad27502440057a199ef09505be0b3ca9e", null ],
      [ "LeftWinger", "namespace_football_management_1_1_program_1_1_data.html#af0ee946918ec0d1119ab67b27df4bd7aa2bf08f9fa414b1714f4e9ad300439d78", null ],
      [ "CentreForward", "namespace_football_management_1_1_program_1_1_data.html#af0ee946918ec0d1119ab67b27df4bd7aa62234cbae29ef6eaa58f5fb0493cbada", null ],
      [ "CentralMidfield", "namespace_football_management_1_1_program_1_1_data.html#af0ee946918ec0d1119ab67b27df4bd7aa2ce45bba8a2f7b7ab63a4545ca82f39c", null ],
      [ "CentreBack", "namespace_football_management_1_1_program_1_1_data.html#af0ee946918ec0d1119ab67b27df4bd7aa349c309210b2f7de8bcb5911454e9e46", null ],
      [ "LeftBack", "namespace_football_management_1_1_program_1_1_data.html#af0ee946918ec0d1119ab67b27df4bd7aacdbccf06631f22cd5a783dae4bdb619b", null ],
      [ "LeftMidfield", "namespace_football_management_1_1_program_1_1_data.html#af0ee946918ec0d1119ab67b27df4bd7aafa984525667322c31ee9aeb7cc8c3688", null ]
    ] ]
];