﻿// <copyright file="PlayerListViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// PlayerListViewModel class.
    /// </summary>
    public class PlayerListViewModel
    {
        /// <summary>
        /// Gets or sets List Of Players.
        /// </summary>
        public List<Player> ListOfPlayers { get; set; }

        /// <summary>
        /// Gets or sets edited Player.
        /// </summary>
        public Player EditedPlayer { get; set; }
    }
}
