﻿// <copyright file="PlayerEditorWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Program.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using FootballManagement.Program.Data;
    using FootballManagement.Program.VM;

    /// <summary>
    /// Interaction logic for PlayerEditorWindow.xaml.
    /// </summary>
    public partial class PlayerEditorWindow : Window
    {
        private PlayerEditorViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerEditorWindow"/> class.
        /// </summary>
        public PlayerEditorWindow()
        {
            this.InitializeComponent();

            this.vM = this.FindResource("VM") as PlayerEditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerEditorWindow"/> class.
        /// </summary>
        /// <param name="oldPlayer">An input player.</param>
        public PlayerEditorWindow(Player oldPlayer)
            : this()
        {
            this.vM.Player = oldPlayer;
        }

        private void OkClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
