﻿// <copyright file="PlayerWithContract.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Logic.ReturnClasses
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// PlayerWithContract class for NON-CRUDS.
    /// </summary>
    public class PlayerWithContract
    {
        /// <summary>
        /// Gets or sets Name of the Player.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        ///  Gets or sets age of player.
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        ///  Gets or sets number of Contract years.
        /// </summary>
        public int ContractYears { get; set; }

        /// <summary>
        ///  Gets or sets Date of contract.
        /// </summary>
        public string ContractDate { get; set; }

        /// <summary>
        /// Overrided toString, to show noncrud.
        /// </summary>
        /// <returns>Formed string.</returns>
        public override string ToString()
        {
            return $"{this.PlayerName} - Age: {this.Age} - ContractDate: [{this.ContractDate}] - Years: {this.ContractYears}";
        }

        /// <summary>
        /// Overrided Equals.
        /// </summary>
        /// <param name="obj">An object.</param>
        /// <returns>Returns true or false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is PlayerWithContract)
            {
                PlayerWithContract other = obj as PlayerWithContract;
                return this.Age == other.Age && this.PlayerName == other.PlayerName && this.ContractDate == other.ContractDate && this.ContractYears == other.ContractYears;
            }

            return false;
        }

        /// <summary>
        /// Overrided GetHashCode.
        /// </summary>
        /// <returns>Obect hashcode.</returns>
        public override int GetHashCode()
        {
            return this.PlayerName.GetHashCode(System.StringComparison.CurrentCulture) + (int)this.Age + this.ContractDate.GetHashCode(System.StringComparison.CurrentCulture) + (int)this.ContractYears;
        }
    }
}
