var class_football_management_1_1_program_1_1_data_1_1_club =
[
    [ "CopyFrom", "class_football_management_1_1_program_1_1_data_1_1_club.html#a9be874bcfdb3eaf99727af31a927b46d", null ],
    [ "ChampionshipWins", "class_football_management_1_1_program_1_1_data_1_1_club.html#a7ae62142624b4aa8c4f4c0db8e940f87", null ],
    [ "ClubId", "class_football_management_1_1_program_1_1_data_1_1_club.html#a5ef828a82756f1079152f4859982d69b", null ],
    [ "Coach", "class_football_management_1_1_program_1_1_data_1_1_club.html#afb85ecfd894187c6897800ab1e428bac", null ],
    [ "Country", "class_football_management_1_1_program_1_1_data_1_1_club.html#af2f8a6a356dbdb49e23a246074342b89", null ],
    [ "Founded", "class_football_management_1_1_program_1_1_data_1_1_club.html#a7ec9b9b3ab37577ff1e000ac05e5792a", null ],
    [ "Name", "class_football_management_1_1_program_1_1_data_1_1_club.html#af7c7228eed56cb5336e9a8760a668b80", null ]
];