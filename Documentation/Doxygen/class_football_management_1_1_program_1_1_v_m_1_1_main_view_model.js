var class_football_management_1_1_program_1_1_v_m_1_1_main_view_model =
[
    [ "MainViewModel", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html#aa2f394aa5f645e5ba6c6ec846c83f843", null ],
    [ "MainViewModel", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html#acc84c647b4a3571fe48aad0e4c48acb9", null ],
    [ "ClubAddCmd", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html#afd5417f6f8c2ad3c7cb64d231a666c6e", null ],
    [ "ClubDelCmd", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html#af93322521d622cab3ab6885f39ee3fd7", null ],
    [ "ClubModCmd", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html#a7ceb983130b8bcfd46bce1691badd85d", null ],
    [ "Clubs", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html#abc0ed23563f24d072b8efd22fad5def7", null ],
    [ "ClubSelected", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html#ab1c43c476df17478d2e0d715297a48e3", null ],
    [ "ContractAddCmd", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html#a2a004631a46eaed8fbb76e5e04fb0f4d", null ],
    [ "ContractDelCmd", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html#a7605d7c3706851dd875337953948dece", null ],
    [ "ContractModCmd", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html#a0b7f364ff990263425223db79bf924a5", null ],
    [ "Contracts", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html#ad1ea35f7b09b2fb882d88cb94e31a6f1", null ],
    [ "ContractSelected", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html#a64955c12fbc0e00f6cd0008fe0883247", null ],
    [ "PlayerAddCmd", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html#a8c792bbb49ab541e3e2a3b55ef90453c", null ],
    [ "PlayerDelCmd", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html#a8a24a3497024bf61de45afbba5e7f5f8", null ],
    [ "PlayerModCmd", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html#ab7b22d09a84e0786ef64e05aeee04ace", null ],
    [ "Players", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html#a8bcfa6f561b41e046d31ca4c125e893f", null ],
    [ "PlayerSelected", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html#a86a348cae4a1e7aa9fc32016fff8beab", null ]
];