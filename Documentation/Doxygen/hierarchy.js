var hierarchy =
[
    [ "FootballManagement.Web.Controllers.PlayersApiController.ApiResult", "class_football_management_1_1_web_1_1_controllers_1_1_players_api_controller_1_1_api_result.html", null ],
    [ "Application", null, [
      [ "FootballManagement.Program.App", "class_football_management_1_1_program_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "FootballManagement.Program.App", "class_football_management_1_1_program_1_1_app.html", null ],
      [ "FootballManagement.Program.App", "class_football_management_1_1_program_1_1_app.html", null ],
      [ "FootballManagement.Program.App", "class_football_management_1_1_program_1_1_app.html", null ],
      [ "FootballManagement.WpfClient.App", "class_football_management_1_1_wpf_client_1_1_app.html", null ],
      [ "FootballManagement.WpfClient.App", "class_football_management_1_1_wpf_client_1_1_app.html", null ],
      [ "FootballManagement.WpfClient.App", "class_football_management_1_1_wpf_client_1_1_app.html", null ]
    ] ],
    [ "Attribute", null, [
      [ "FootballManagement.Data.Model.ToStringAttribute", "class_football_management_1_1_data_1_1_model_1_1_to_string_attribute.html", null ]
    ] ],
    [ "FootballManagement.Data.Club", "class_football_management_1_1_data_1_1_club.html", null ],
    [ "FootballManagement.Logic.ReturnClasses.ClubGoals", "class_football_management_1_1_logic_1_1_return_classes_1_1_club_goals.html", null ],
    [ "FootballManagement.Logic.Tests.ClubLeaderLogicTest", "class_football_management_1_1_logic_1_1_tests_1_1_club_leader_logic_test.html", null ],
    [ "FootballManagement.Data.Contract", "class_football_management_1_1_data_1_1_contract.html", null ],
    [ "FootballManagement.Logic.Tests.ContractManagementLogicTest", "class_football_management_1_1_logic_1_1_tests_1_1_contract_management_logic_test.html", null ],
    [ "Controller", null, [
      [ "FootballManagement.Web.Controllers.HomeController", "class_football_management_1_1_web_1_1_controllers_1_1_home_controller.html", null ],
      [ "FootballManagement.Web.Controllers.PlayersApiController", "class_football_management_1_1_web_1_1_controllers_1_1_players_api_controller.html", null ],
      [ "FootballManagement.Web.Controllers.PlayersController", "class_football_management_1_1_web_1_1_controllers_1_1_players_controller.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "FootballManagement.Data.FootballContext", "class_football_management_1_1_data_1_1_football_context.html", null ]
    ] ],
    [ "FootballManagement.Web.Models.ErrorViewModel", "class_football_management_1_1_web_1_1_models_1_1_error_view_model.html", null ],
    [ "FootballManagement.Repository.FootballRepository< Club >", "class_football_management_1_1_repository_1_1_football_repository.html", [
      [ "FootballManagement.Repository.ClubRepository", "class_football_management_1_1_repository_1_1_club_repository.html", null ]
    ] ],
    [ "FootballManagement.Repository.FootballRepository< Contract >", "class_football_management_1_1_repository_1_1_football_repository.html", [
      [ "FootballManagement.Repository.ContractRepository", "class_football_management_1_1_repository_1_1_contract_repository.html", null ]
    ] ],
    [ "FootballManagement.Repository.FootballRepository< Player >", "class_football_management_1_1_repository_1_1_football_repository.html", [
      [ "FootballManagement.Repository.PlayerRepository", "class_football_management_1_1_repository_1_1_player_repository.html", null ]
    ] ],
    [ "FootballManagement.Logic.IClubLeaderLogic", "interface_football_management_1_1_logic_1_1_i_club_leader_logic.html", [
      [ "FootballManagement.Logic.ClubLeaderLogic", "class_football_management_1_1_logic_1_1_club_leader_logic.html", null ]
    ] ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "FootballManagement.Program.MainWindow", "class_football_management_1_1_program_1_1_main_window.html", null ],
      [ "FootballManagement.Program.MainWindow", "class_football_management_1_1_program_1_1_main_window.html", null ],
      [ "FootballManagement.Program.MainWindow", "class_football_management_1_1_program_1_1_main_window.html", null ],
      [ "FootballManagement.Program.UI.ClubEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_club_editor_window.html", null ],
      [ "FootballManagement.Program.UI.ClubEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_club_editor_window.html", null ],
      [ "FootballManagement.Program.UI.ContractEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_contract_editor_window.html", null ],
      [ "FootballManagement.Program.UI.ContractEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_contract_editor_window.html", null ],
      [ "FootballManagement.Program.UI.EditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_editor_window.html", null ],
      [ "FootballManagement.Program.UI.EditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_editor_window.html", null ],
      [ "FootballManagement.Program.UI.PlayerEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_player_editor_window.html", null ],
      [ "FootballManagement.Program.UI.PlayerEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_player_editor_window.html", null ],
      [ "FootballManagement.WpfClient.EditorWindow", "class_football_management_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "FootballManagement.WpfClient.EditorWindow", "class_football_management_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "FootballManagement.WpfClient.MainWindow", "class_football_management_1_1_wpf_client_1_1_main_window.html", null ],
      [ "FootballManagement.WpfClient.MainWindow", "class_football_management_1_1_wpf_client_1_1_main_window.html", null ]
    ] ],
    [ "FootballManagement.Logic.IContractManagementLogic", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html", [
      [ "FootballManagement.Logic.ContractManagementLogic", "class_football_management_1_1_logic_1_1_contract_management_logic.html", null ]
    ] ],
    [ "FootballManagement.Program.BL.IEditorService", "interface_football_management_1_1_program_1_1_b_l_1_1_i_editor_service.html", [
      [ "FootballManagement.Program.UI.EditorServiceViaWindow", "class_football_management_1_1_program_1_1_u_i_1_1_editor_service_via_window.html", null ]
    ] ],
    [ "FootballManagement.Program.BL.IFootballLogic", "interface_football_management_1_1_program_1_1_b_l_1_1_i_football_logic.html", [
      [ "FootballManagement.Program.BL.FootballLogic", "class_football_management_1_1_program_1_1_b_l_1_1_football_logic.html", null ]
    ] ],
    [ "FootballManagement.Repository.IFootballRepository< T >", "interface_football_management_1_1_repository_1_1_i_football_repository.html", [
      [ "FootballManagement.Repository.FootballRepository< T >", "class_football_management_1_1_repository_1_1_football_repository.html", null ]
    ] ],
    [ "FootballManagement.Repository.IFootballRepository< Club >", "interface_football_management_1_1_repository_1_1_i_football_repository.html", [
      [ "FootballManagement.Repository.Interfaces.IClubRepository", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_club_repository.html", [
        [ "FootballManagement.Repository.ClubRepository", "class_football_management_1_1_repository_1_1_club_repository.html", null ]
      ] ]
    ] ],
    [ "FootballManagement.Repository.IFootballRepository< Contract >", "interface_football_management_1_1_repository_1_1_i_football_repository.html", [
      [ "FootballManagement.Repository.Interfaces.IContractRepository", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_contract_repository.html", [
        [ "FootballManagement.Repository.ContractRepository", "class_football_management_1_1_repository_1_1_contract_repository.html", null ]
      ] ]
    ] ],
    [ "FootballManagement.Repository.IFootballRepository< Player >", "interface_football_management_1_1_repository_1_1_i_football_repository.html", [
      [ "FootballManagement.Repository.Interfaces.IPlayerRepository", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_player_repository.html", [
        [ "FootballManagement.Repository.PlayerRepository", "class_football_management_1_1_repository_1_1_player_repository.html", null ]
      ] ]
    ] ],
    [ "FootballManagement.WpfClient.IMainLogic", "interface_football_management_1_1_wpf_client_1_1_i_main_logic.html", [
      [ "FootballManagement.WpfClient.MainLogic", "class_football_management_1_1_wpf_client_1_1_main_logic.html", null ]
    ] ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "IServiceLocator", null, [
      [ "FootballManagement.Program.MyIoc", "class_football_management_1_1_program_1_1_my_ioc.html", null ],
      [ "FootballManagement.WpfClient.App.MyIoc", "class_football_management_1_1_wpf_client_1_1_app_1_1_my_ioc.html", null ]
    ] ],
    [ "FootballManagement.Web.Models.MapperFactory", "class_football_management_1_1_web_1_1_models_1_1_mapper_factory.html", null ],
    [ "FootballManagement.Logic.ReturnClasses.MostWins", "class_football_management_1_1_logic_1_1_return_classes_1_1_most_wins.html", null ],
    [ "ObservableObject", null, [
      [ "FootballManagement.Program.Data.Club", "class_football_management_1_1_program_1_1_data_1_1_club.html", null ],
      [ "FootballManagement.Program.Data.Contract", "class_football_management_1_1_program_1_1_data_1_1_contract.html", null ],
      [ "FootballManagement.Program.Data.Player", "class_football_management_1_1_program_1_1_data_1_1_player.html", null ],
      [ "FootballManagement.WpfClient.PlayerVM", "class_football_management_1_1_wpf_client_1_1_player_v_m.html", null ]
    ] ],
    [ "FootballManagement.Data.Player", "class_football_management_1_1_data_1_1_player.html", null ],
    [ "FootballManagement.Web.Models.Player", "class_football_management_1_1_web_1_1_models_1_1_player.html", null ],
    [ "FootballManagement.Web.Models.PlayerListViewModel", "class_football_management_1_1_web_1_1_models_1_1_player_list_view_model.html", null ],
    [ "FootballManagement.Logic.ReturnClasses.PlayerWithContract", "class_football_management_1_1_logic_1_1_return_classes_1_1_player_with_contract.html", null ],
    [ "FootballManagement.Web.Program", "class_football_management_1_1_web_1_1_program.html", null ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage", null, [
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Players_PlayersDetails", "class_asp_net_core_1_1_views___players___players_details.html", null ],
      [ "AspNetCore.Views_Players_PlayersEdit", "class_asp_net_core_1_1_views___players___players_edit.html", null ],
      [ "AspNetCore.Views_Players_PlayersIndex", "class_asp_net_core_1_1_views___players___players_index.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IEnumerable< FootballManagement.Web.Models.Player >>", null, [
      [ "AspNetCore.Views_Players_PlayersList", "class_asp_net_core_1_1_views___players___players_list.html", null ]
    ] ],
    [ "FootballManagement.Logic.ReturnClasses.Salaries", "class_football_management_1_1_logic_1_1_return_classes_1_1_salaries.html", null ],
    [ "SimpleIoc", null, [
      [ "FootballManagement.Program.MyIoc", "class_football_management_1_1_program_1_1_my_ioc.html", null ],
      [ "FootballManagement.WpfClient.App.MyIoc", "class_football_management_1_1_wpf_client_1_1_app_1_1_my_ioc.html", null ]
    ] ],
    [ "FootballManagement.Web.Startup", "class_football_management_1_1_web_1_1_startup.html", null ],
    [ "ViewModelBase", null, [
      [ "FootballManagement.Program.VM.ClubEditorViewModel", "class_football_management_1_1_program_1_1_v_m_1_1_club_editor_view_model.html", null ],
      [ "FootballManagement.Program.VM.ContractEditorViewModel", "class_football_management_1_1_program_1_1_v_m_1_1_contract_editor_view_model.html", null ],
      [ "FootballManagement.Program.VM.MainViewModel", "class_football_management_1_1_program_1_1_v_m_1_1_main_view_model.html", null ],
      [ "FootballManagement.Program.VM.PlayerEditorViewModel", "class_football_management_1_1_program_1_1_v_m_1_1_player_editor_view_model.html", null ],
      [ "FootballManagement.WpfClient.MainViewModel", "class_football_management_1_1_wpf_client_1_1_main_view_model.html", null ]
    ] ],
    [ "System.Windows.Window", null, [
      [ "FootballManagement.Program.MainWindow", "class_football_management_1_1_program_1_1_main_window.html", null ],
      [ "FootballManagement.Program.MainWindow", "class_football_management_1_1_program_1_1_main_window.html", null ],
      [ "FootballManagement.Program.MainWindow", "class_football_management_1_1_program_1_1_main_window.html", null ],
      [ "FootballManagement.Program.UI.ClubEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_club_editor_window.html", null ],
      [ "FootballManagement.Program.UI.ClubEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_club_editor_window.html", null ],
      [ "FootballManagement.Program.UI.ClubEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_club_editor_window.html", null ],
      [ "FootballManagement.Program.UI.ContractEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_contract_editor_window.html", null ],
      [ "FootballManagement.Program.UI.ContractEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_contract_editor_window.html", null ],
      [ "FootballManagement.Program.UI.ContractEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_contract_editor_window.html", null ],
      [ "FootballManagement.Program.UI.EditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_editor_window.html", null ],
      [ "FootballManagement.Program.UI.EditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_editor_window.html", null ],
      [ "FootballManagement.Program.UI.PlayerEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_player_editor_window.html", null ],
      [ "FootballManagement.Program.UI.PlayerEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_player_editor_window.html", null ],
      [ "FootballManagement.Program.UI.PlayerEditorWindow", "class_football_management_1_1_program_1_1_u_i_1_1_player_editor_window.html", null ],
      [ "FootballManagement.WpfClient.EditorWindow", "class_football_management_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "FootballManagement.WpfClient.EditorWindow", "class_football_management_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "FootballManagement.WpfClient.EditorWindow", "class_football_management_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "FootballManagement.WpfClient.MainWindow", "class_football_management_1_1_wpf_client_1_1_main_window.html", null ],
      [ "FootballManagement.WpfClient.MainWindow", "class_football_management_1_1_wpf_client_1_1_main_window.html", null ],
      [ "FootballManagement.WpfClient.MainWindow", "class_football_management_1_1_wpf_client_1_1_main_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "FootballManagement.Program.MainWindow", "class_football_management_1_1_program_1_1_main_window.html", null ]
    ] ]
];