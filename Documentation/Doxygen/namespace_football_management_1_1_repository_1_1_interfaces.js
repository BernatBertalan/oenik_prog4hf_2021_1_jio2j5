var namespace_football_management_1_1_repository_1_1_interfaces =
[
    [ "IClubRepository", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_club_repository.html", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_club_repository" ],
    [ "IContractRepository", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_contract_repository.html", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_contract_repository" ],
    [ "IPlayerRepository", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_player_repository.html", "interface_football_management_1_1_repository_1_1_interfaces_1_1_i_player_repository" ]
];