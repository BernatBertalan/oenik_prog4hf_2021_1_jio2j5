﻿// <copyright file="ContractManagementLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballManagement.Data;
    using FootballManagement.Logic.ReturnClasses;
    using FootballManagement.Repository.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test ContractManagementLogic cruds and non cruds.
    /// </summary>
    [TestFixture]
    public class ContractManagementLogicTest
    {
        private static List<Player> players = new List<Player>()
        {
                new Player() { PlayerId = 1, Name = "Lionel Messi", Age = 33, Goals = 706, Nation = "Argentina", Position = "Right Winger" },
                new Player() { PlayerId = 2, Name = "Cristiano Ronaldo", Age = 35, Goals = 742, Nation = "Portugal", Position = "Left Winger" },
                new Player() { PlayerId = 3, Name = "Szoboszlai Dominik", Age = 19, Goals = 39, Nation = "Hungary", Position = "Left Midfield" },
                new Player() { PlayerId = 4, Name = "Luis Suárez", Age = 33, Goals = 467, Nation = "Uruguay", Position = "Centre-Forward" },
                new Player() { PlayerId = 5, Name = "Robert Lewandowski", Age = 32, Goals = 477, Nation = "Poland", Position = "Centre-Forward" },
                new Player() { PlayerId = 6, Name = "Andrés Iniesta", Age = 36, Goals = 84, Nation = "Spain", Position = "Central Midfield" },
                new Player() { PlayerId = 7, Name = "Marcelo", Age = 32, Goals = 47, Nation = "Brazil", Position = "Left-Back" },
                new Player() { PlayerId = 8, Name = "Virgil van Dijk", Age = 29, Goals = 47, Nation = "Netherlands", Position = "Centre-Back" },
                new Player() { PlayerId = 9, Name = "Neymar Junior", Age = 28, Goals = 311, Nation = "Brazil", Position = "Left Winger" },
                new Player() { PlayerId = 10, Name = "Zlatan Ibrahimović", Age = 39, Goals = 532, Nation = "Sweeden", Position = "Centre-Forward" },
                new Player() { PlayerId = 11, Name = "David Neres", Age = 23, Goals = 39, Nation = "Brazil", Position = "Right Winger" },
        };

        private static List<Contract> contracts = new List<Contract>()
        {
                new Contract() { PlayerId = 1,  ClubId = 1, ContractYears = 1, ContractDate = "2018.09.30", Salary = 45000000, ReleaseClause = 700000000, Agent = "Jorge Mendes" },
                new Contract() { PlayerId = 2, ClubId = 2, ContractYears = 3, ContractDate = "2017.11.13", Salary = 40000000, ReleaseClause = 500000000, Agent = "Jorge Mendes" },
                new Contract() { PlayerId = 3, ClubId = 3, ContractYears = 3, ContractDate = "2019.09.25", Salary = 2000000, ReleaseClause = 20000000, Agent = "Mino Raiola" },
                new Contract() { PlayerId = 4, ClubId = 4, ContractYears = 1, ContractDate = "2020.09.31", Salary = 23200000, ReleaseClause = 200000000, Agent = "Jonathan Barnett" },
                new Contract() { PlayerId = 5, ClubId = 5, ContractYears = 4, ContractDate = "2019.11.24", Salary = 27600000, ReleaseClause = 400000000, Agent = "Juan Figer" },
                new Contract() { PlayerId = 6, ClubId = 6, ContractYears = 1, ContractDate = "2018.09.30", Salary = 25000000, ReleaseClause = 100000000, Agent = "Pep Guardiola" },
                new Contract() { PlayerId = 7, ClubId = 7, ContractYears = 1, ContractDate = "2018.09.30", Salary = 6750000, ReleaseClause = 80000000, Agent = "Juan Figer" },
                new Contract() { PlayerId = 8, ClubId = 8, ContractYears = 1, ContractDate = "2018.09.30", Salary = 11460000, ReleaseClause = 140000000, Agent = "Jonathan Barnett" },
                new Contract() { PlayerId = 9, ClubId = 9, ContractYears = 1, ContractDate = "2018.09.30", Salary = 3580000, ReleaseClause = 400000000, Agent = "Jorge Mendes" },
                new Contract() { PlayerId = 10, ClubId = 10, ContractYears = 1, ContractDate = "2018.09.30", Salary = 45000000, ReleaseClause = 150000000, Agent = "Mino Raiola" },
                new Contract() { PlayerId = 11, ClubId = 11, ContractYears = 1, ContractDate = "2018.09.30", Salary = 45000000, ReleaseClause = 38000000, Agent = "Jorge Mendes" },
        };

        /// <summary>
        /// Tests get player by id crud method.
        /// </summary>
        [Test]
        public void TestGetOnePlayer()
        {
            Mock<IPlayerRepository> mrepo = new Mock<IPlayerRepository>();
            Mock<IContractRepository> mcrepo = new Mock<IContractRepository>();

            Player expected = new Player() { PlayerId = 1, Name = "Lionel Messi", Age = 33, Goals = 706, Nation = "Argentina", Position = "Right Winger" };
            mrepo.Setup(x => x.GetOne(1)).Returns(players[0]);
            ContractManagementLogic logic = new ContractManagementLogic(mcrepo.Object, mrepo.Object);

            var logicget = logic.GetPlayerById(1);

            mrepo.Verify(x => x.GetOne(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Tests contracts with less than two years contract years noncrud method.
        /// </summary>
        [Test]
        public void TestLessThanTwoYearContract()
        {
            Mock<IPlayerRepository> mprepo = new Mock<IPlayerRepository>();
            Mock<IContractRepository> mcrepo = new Mock<IContractRepository>();

            List<PlayerWithContract> expected = new List<PlayerWithContract>()
            {
                new PlayerWithContract() { PlayerName = "Lionel Messi", Age = 33, ContractDate = "2018.09.30", ContractYears = 1 },
                new PlayerWithContract() { PlayerName = "Luis Suárez", Age = 33, ContractDate = "2020.09.31", ContractYears = 1 },
                new PlayerWithContract() { PlayerName = "Andrés Iniesta", Age = 36, ContractDate = "2018.09.30", ContractYears = 1 },
                new PlayerWithContract() { PlayerName = "Marcelo", Age = 32, ContractDate = "2018.09.30", ContractYears = 1 },
                new PlayerWithContract() { PlayerName = "Virgil van Dijk", Age = 29, ContractDate = "2018.09.30", ContractYears = 1 },
                new PlayerWithContract() { PlayerName = "Neymar Junior", Age = 28, ContractDate = "2018.09.30", ContractYears = 1 },
                new PlayerWithContract() { PlayerName = "Zlatan Ibrahimović", Age = 39, ContractDate = "2018.09.30", ContractYears = 1 },
                new PlayerWithContract() { PlayerName = "David Neres", Age = 23, ContractDate = "2018.09.30", ContractYears = 1 },
            };

            mprepo.Setup(x => x.GetAll()).Returns(players.AsQueryable());
            mcrepo.Setup(x => x.GetAll()).Returns(contracts.AsQueryable());

            ContractManagementLogic logic = new ContractManagementLogic(mcrepo.Object, mprepo.Object);
            var playercontracts = logic.LessThanTwoYearContract();

            Assert.That(playercontracts, Is.EquivalentTo(expected));
            Assert.That(playercontracts.Count, Is.EqualTo(expected.Count));

            mprepo.Verify(x => x.GetAll(), Times.Once);
            mcrepo.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests players with the highest salaries noncrud method.
        /// </summary>
        [Test]
        public void TestTopFiveHighestSalariesOver30()
        {
            Mock<IPlayerRepository> mprepo = new Mock<IPlayerRepository>();
            Mock<IContractRepository> mcrepo = new Mock<IContractRepository>();

            List<Salaries> expected = new List<Salaries>()
            {
                new Salaries() { PlayerName = "Lionel Messi", Salary = 45000000 },
                new Salaries() { PlayerName = "Zlatan Ibrahimović", Salary = 45000000 },
                new Salaries() { PlayerName = "Cristiano Ronaldo", Salary = 40000000 },
                new Salaries() { PlayerName = "Robert Lewandowski", Salary = 27600000 },
                new Salaries() { PlayerName = "Andrés Iniesta", Salary = 25000000 },
            };

            mprepo.Setup(x => x.GetAll()).Returns(players.AsQueryable());
            mcrepo.Setup(x => x.GetAll()).Returns(contracts.AsQueryable());

            ContractManagementLogic logic = new ContractManagementLogic(mcrepo.Object, mprepo.Object);
            var salaries = logic.TopFiveHighestSalariesOver30();

            Assert.That(salaries, Is.EquivalentTo(expected));
            Assert.That(salaries.Count, Is.EqualTo(expected.Count));

            mprepo.Verify(x => x.GetAll(), Times.Once);
            mcrepo.Verify(x => x.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests delete player crud method.
        /// </summary>
        [Test]
        public void TestDeletePlayer()
        {
            Mock<IPlayerRepository> mprepo = new Mock<IPlayerRepository>();
            Mock<IContractRepository> mcrepo = new Mock<IContractRepository>();

            mprepo.Setup(x => x.Delete(1));
            ContractManagementLogic logic = new ContractManagementLogic(mcrepo.Object, mprepo.Object);

            logic.Delete(1);

            mprepo.Verify(x => x.Delete(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// Tests Add new Player curd method.
        /// </summary>
        [Test]
        public void TestAddNewPlayer()
        {
            Mock<IPlayerRepository> mprepo = new Mock<IPlayerRepository>();
            Mock<IContractRepository> mcrepo = new Mock<IContractRepository>();

            mprepo.Setup(x => x.AddPlayer("Tokmac", 25, 130, "Hungarian", "Centre-Forward"));
            ContractManagementLogic logic = new ContractManagementLogic(mcrepo.Object, mprepo.Object);

            logic.AddPlayer("Tokmac", 25, 130, "Hungarian", "Centre-Forward");

            mprepo.Verify(x => x.AddPlayer(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }
    }
}
