﻿// <copyright file="ContractRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballManagement.Data;
    using FootballManagement.Repository.Interfaces;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// ContractRepository.
    /// </summary>
    public class ContractRepository : FootballRepository<Contract>, IContractRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ContractRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext context.</param>
        public ContractRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Change the player agent.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newAgent">New player agent.</param>
        public void ChangeAgent(int playerid, int clubid, string newAgent)
        {
            if (this.GetOne(playerid, clubid) != null)
            {
                var contract = this.GetOne(playerid, clubid);
                contract.Agent = newAgent;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This Contract is not exist!");
            }
        }

        /// <summary>
        /// Change date of contract.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newContractDate">New date of contract.</param>
        public void ChangeContractDate(int playerid, int clubid, string newContractDate)
        {
            if (this.GetOne(playerid, clubid) != null)
            {
                var contract = this.GetOne(playerid, clubid);
                contract.ContractDate = newContractDate;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This Contract is not exist!");
            }
        }

        /// <summary>
        /// Change number of Contract years.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newContractYears">New number of Contract years.</param>
        public void ChangeContractYears(int playerid, int clubid, int newContractYears)
        {
            if (this.GetOne(playerid, clubid) != null)
            {
                var contract = this.GetOne(playerid, clubid);
                contract.ContractYears = newContractYears;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This Contract is not exist!");
            }
        }

        /// <summary>
        /// Change the Release clause.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newReleaseClause">New Release clause of the player.</param>
        public void ChangeReleaseClause(int playerid, int clubid, int newReleaseClause)
        {
            if (this.GetOne(playerid, clubid) != null)
            {
                var contract = this.GetOne(playerid, clubid);
                contract.ReleaseClause = newReleaseClause;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This Contract is not exist!");
            }
        }

        /// <summary>
        /// Change the Salary of the player.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newSalary">New value of Salary.</param>
        public void ChangeSalary(int playerid, int clubid, int newSalary)
        {
            if (this.GetOne(playerid, clubid) != null)
            {
                var contract = this.GetOne(playerid, clubid);
                contract.Salary = newSalary;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This Contract is not exist!");
            }
        }

        /// <summary>
        /// Delete a row.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        public void Delete(int playerid, int clubid)
        {
            if (this.GetOne(playerid, clubid) != null)
            {
                var contract = this.GetOne(playerid, clubid);
                this.Ctx.Remove(contract);
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This Contract is not exist!");
            }
        }

        /// <summary>
        /// Get Contract By Id.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <returns>Returns the contract with the given id's.</returns>
        public Contract GetOne(int playerid, int clubid)
        {
            return this.GetAll().SingleOrDefault(x => x.ClubId == clubid && x.PlayerId == playerid);
        }

        /// <summary>
        /// Add a new Contract.
        /// </summary>
        /// <param name="playerid">Id of the player you wants to add.</param>
        /// <param name="clubid">Id of the clubs who signed the player.</param>
        /// <param name="contractyears">Contract years.</param>
        /// <param name="contractdate">Date of the contract.</param>
        /// <param name="salary">Salary in euro.</param>
        /// <param name="releaseclause">Release Clause in euro.</param>
        /// <param name="agent">Player Agent.</param>
        public void AddContract(int playerid, int clubid, int contractyears, string contractdate, int salary, int releaseclause, string agent)
        {
            try
            {
                this.Ctx.Add(new Contract() { PlayerId = playerid, ClubId = clubid, ContractYears = contractyears, ContractDate = contractdate, Salary = salary, ReleaseClause = releaseclause, Agent = agent });
                this.Ctx.SaveChanges();
            }
            catch (DbUpdateException)
            {
                throw new DbUpdateException("Club and/or Player ID not exist!");
            }
        }
    }
}
