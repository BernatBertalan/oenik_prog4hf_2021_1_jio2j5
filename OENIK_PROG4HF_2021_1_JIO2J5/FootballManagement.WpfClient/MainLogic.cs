﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace FootballManagement.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// MainLogic class.
    /// </summary>
    public class MainLogic : IMainLogic
    {
        private string url = "https://localhost:44303/PlayersApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// Sends message, succesful or not.
        /// </summary>
        /// <param name="success">True or false.</param>
        public void SendMessage(bool success)
        {
            string msg = success ? "Operation completed succesfully" : "Operation Failed";
            Messenger.Default.Send(msg, "PlayerResult");
        }

        /// <summary>
        /// Calls PlayersApi controller getall.
        /// </summary>
        /// <returns>List of players.</returns>
        public List<PlayerVM> ApiGetPlayers()
        {
            string json = this.client.GetStringAsync(this.url + "all").Result;
            var list = JsonSerializer.Deserialize<List<PlayerVM>>(json, this.jsonOptions);
            return list;
        }

        /// <summary>
        /// Calls PlayersApi controller delete.
        /// </summary>
        /// <param name="player">Player we want to edit.</param>
        public void ApiDelPlayer(PlayerVM player)
        {
            bool success = false;

            if (player != null)
            {
                string json = this.client.GetStringAsync(this.url + "del/" + player.PlayerId.ToString()).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            this.SendMessage(success);
        }

        /// <summary>
        /// Calls PlayersApi controller edit.
        /// </summary>
        /// <param name="player">Player we want to edit.</param>
        /// <param name="isEditing">Is editing now.</param>
        /// <returns>True or false.</returns>
        public bool ApiEditPlayer(PlayerVM player, bool isEditing)
        {
            if (player == null)
            {
                return false;
            }

            string myUrl = this.url + (isEditing ? "mod" : "add");

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("playerid", player.PlayerId.ToString());
            }

            postData.Add("name", player.Name.ToString());
            postData.Add("age", player.Age.ToString());
            postData.Add("goals", player.Goals.ToString());
            postData.Add("nation", player.Nation.ToString());
            postData.Add("position", player.Position.ToString());

            string json = this.client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }

        /// <summary>
        /// Implements it is add or edit.
        /// </summary>
        /// <param name="player">Player we want to edit.</param>
        /// <param name="editorFunc">Func that will be implement in MainWindow.</param>
        public void EditPlayer(PlayerVM player, Func<PlayerVM, bool> editorFunc)
        {
            PlayerVM clone = new PlayerVM();
            if (player != null)
            {
                clone.CopyFrom(player);
            }

            bool? success = editorFunc?.Invoke(clone);
            if (success == true)
            {
                if (player != null)
                {
                    success = this.ApiEditPlayer(clone, true);
                }
                else
                {
                    success = this.ApiEditPlayer(clone, false);
                }
            }

            this.SendMessage(success == true);
        }
    }
}
