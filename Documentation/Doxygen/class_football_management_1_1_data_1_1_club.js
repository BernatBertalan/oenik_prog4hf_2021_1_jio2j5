var class_football_management_1_1_data_1_1_club =
[
    [ "Club", "class_football_management_1_1_data_1_1_club.html#ae35284d059b97c0a858f2aac2062ab29", null ],
    [ "Equals", "class_football_management_1_1_data_1_1_club.html#a2220c2e9b1edd4b8470352e382087437", null ],
    [ "GetHashCode", "class_football_management_1_1_data_1_1_club.html#a11c1d29b77df3ae764958ea0101d0e66", null ],
    [ "ToString", "class_football_management_1_1_data_1_1_club.html#a67cb95b279264c69b02593b6d5fea0ee", null ],
    [ "ChampionshipWins", "class_football_management_1_1_data_1_1_club.html#af492c4e184b46d7440851df501f63ec3", null ],
    [ "ClubId", "class_football_management_1_1_data_1_1_club.html#ae9b6427c0649a30c8659558e0a5c7d65", null ],
    [ "Coach", "class_football_management_1_1_data_1_1_club.html#a4b563408375e9514a8d85e93c1935d40", null ],
    [ "Contracts", "class_football_management_1_1_data_1_1_club.html#a562117b66564789830b41965282f3051", null ],
    [ "Country", "class_football_management_1_1_data_1_1_club.html#ac2cff421275e7b3dc131f1ce9e279b4b", null ],
    [ "Founded", "class_football_management_1_1_data_1_1_club.html#af8fde69c93f43a1bb20728ad7c12516a", null ],
    [ "MainData", "class_football_management_1_1_data_1_1_club.html#a89b56fcf0aa0b71a3466133e97a00bf0", null ],
    [ "Name", "class_football_management_1_1_data_1_1_club.html#a4314d59cc13fbe600ce9ebb31344b0a7", null ]
];