﻿// <copyright file="PlayerEditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Program.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballManagement.Program.Data;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Implements ViewModelBase.
    /// </summary>
    public class PlayerEditorViewModel : ViewModelBase
    {
        private Player player;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerEditorViewModel"/> class.
        /// </summary>
        public PlayerEditorViewModel()
        {
            this.player = new Player();
            if (this.IsInDesignMode)
            {
                this.player.Name = "Lionel Messi";
                this.player.Goals = 100;
                this.player.Nation = "Argentina";
            }
        }

        /// <summary>
        /// Gets an Array of Positions.
        /// </summary>
        public static Array Positions
        {
            get { return Enum.GetValues(typeof(PositionType)); }
        }

        /// <summary>
        /// Gets or sets a Player to edit.
        /// </summary>
        public Player Player
        {
            get { return this.player; }
            set { this.Set(ref this.player, value); }
        }
    }
}
