﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Program.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using FootballManagement.Program.BL;
    using FootballManagement.Program.Data;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// Implements ViewModelBase class.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private IFootballLogic logic;
        private Player playerselected;
        private Club clubselected;
        private Contract contractselected;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">From IFootballLogic.</param>
        public MainViewModel(IFootballLogic logic)
        {
            this.logic = logic;
            this.Players = new ObservableCollection<Player>();
            this.Clubs = new ObservableCollection<Club>();
            this.Contracts = new ObservableCollection<Contract>();

            this.PlayerAddCmd = new RelayCommand(() => this.logic.AddPlayer(this.Players));
            this.PlayerModCmd = new RelayCommand(() => this.logic.ModPlayer(this.PlayerSelected));
            this.PlayerDelCmd = new RelayCommand(() => this.logic.DelPlayer(this.Players, this.PlayerSelected));

            this.ClubAddCmd = new RelayCommand(() => this.logic.AddClub(this.Clubs));
            this.ClubModCmd = new RelayCommand(() => this.logic.ModClub(this.ClubSelected));
            this.ClubDelCmd = new RelayCommand(() => this.logic.DelClub(this.Clubs, this.ClubSelected));

            this.ContractAddCmd = new RelayCommand(() => this.logic.AddContract(this.Contracts));
            this.ContractModCmd = new RelayCommand(() => this.logic.ModContract(this.ContractSelected));
            this.ContractDelCmd = new RelayCommand(() => this.logic.DelContract(this.Contracts, this.ContractSelected));

            if (!this.IsInDesignMode)
            {
                this.logic.GetAllPlayers().ToList().ForEach(x => this.Players.Add(x));
                this.logic.GetAllContract().ToList().ForEach(x => this.Contracts.Add(x));
                this.logic.GetAllClub().ToList().ForEach(x => this.Clubs.Add(x));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IFootballLogic>())
        {
        }

        /// <summary>
        /// Gets collection of Players.
        /// </summary>
        public ObservableCollection<Player> Players { get; private set; }

        /// <summary>
        /// Gets collection of Clubs.
        /// </summary>
        public ObservableCollection<Club> Clubs { get; private set; }

        /// <summary>
        /// Gets collection of Contracts.
        /// </summary>
        public ObservableCollection<Contract> Contracts { get; private set; }

        /// <summary>
        /// Gets or sets the selectedPlayer.
        /// </summary>
        public Player PlayerSelected
        {
            get { return this.playerselected; }
            set { this.Set(ref this.playerselected, value); }
        }

        /// <summary>
        /// Gets or sets the selectedClub.
        /// </summary>
        public Club ClubSelected
        {
            get { return this.clubselected; }
            set { this.Set(ref this.clubselected, value); }
        }

        /// <summary>
        /// Gets or sets the selectedContract.
        /// </summary>
        public Contract ContractSelected
        {
            get { return this.contractselected; }
            set { this.Set(ref this.contractselected, value); }
        }

        /// <summary>
        /// Gets add a player command.
        /// </summary>
        public ICommand PlayerAddCmd { get; private set; }

        /// <summary>
        /// Gets modify a player command.
        /// </summary>
        public ICommand PlayerModCmd { get; private set; }

        /// <summary>
        /// Gets delete a player command.
        /// </summary>
        public ICommand PlayerDelCmd { get; private set; }

        /// <summary>
        /// Gets add a club command.
        /// </summary>
        public ICommand ClubAddCmd { get; private set; }

        /// <summary>
        /// Gets modify a club command.
        /// </summary>
        public ICommand ClubModCmd { get; private set; }

        /// <summary>
        /// Gets delete a club command.
        /// </summary>
        public ICommand ClubDelCmd { get; private set; }

        /// <summary>
        /// Gets add a contract command.
        /// </summary>
        public ICommand ContractAddCmd { get; private set; }

        /// <summary>
        /// Gets modify a contract command.
        /// </summary>
        public ICommand ContractModCmd { get; private set; }

        /// <summary>
        /// Gets delete a contract command.
        /// </summary>
        public ICommand ContractDelCmd { get; private set; }
    }
}
