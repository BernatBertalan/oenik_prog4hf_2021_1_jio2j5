﻿// <copyright file="FootballLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Program.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballManagement.Logic;
    using FootballManagement.Program.Data;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// Implements IFottballLogic interface.
    /// </summary>
    public class FootballLogic : IFootballLogic
    {
        private IContractManagementLogic contractMgmtLogic;
        private IClubLeaderLogic clubLeaderLogic;

        private IEditorService editorservice;
        private IMessenger messengerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="FootballLogic"/> class.
        /// </summary>
        /// <param name="editorservice">IEditorService param.</param>
        /// <param name="messengerService">IMessenger param.</param>
        /// <param name="logic1">IContractManagementLogic param.</param>
        /// <param name="logic2">IClubLeaderLogic param.</param>
        public FootballLogic(IEditorService editorservice, IMessenger messengerService, IContractManagementLogic logic1, IClubLeaderLogic logic2)
        {
            this.contractMgmtLogic = logic1;
            this.clubLeaderLogic = logic2;

            this.editorservice = editorservice;
            this.messengerService = messengerService;
        }

        /// <summary>
        /// Get all players method.
        /// </summary>
        /// <returns>List of players.</returns>
        public IList<Player> GetAllPlayers()
        {
            List<Player> output = new List<Player>();

            this.contractMgmtLogic.GetAllPlayers().ToList().ForEach(x => output.Add(new Player() { PlayerId = x.PlayerId, Name = x.Name, Age = x.Age, Goals = x.Goals, Nation = x.Nation, Position = (Data.PositionType)Enum.Parse(typeof(Data.PositionType), x.Position) }));

            return output;
        }

        /// <summary>
        /// Get all players method.
        /// </summary>
        /// <returns>List of players.</returns>
        public IList<Club> GetAllClub()
        {
            List<Club> output = new List<Club>();

            this.clubLeaderLogic.GetAllClubs().ToList().ForEach(x => output.Add(new Club() { ClubId = x.ClubId, Name = x.Name, Coach = x.Coach, ChampionshipWins = x.ChampionshipWins, Founded = x.Founded, Country = x.Country }));

            return output;
        }

        /// <summary>
        /// Get all players method.
        /// </summary>
        /// <returns>List of players.</returns>
        public IList<Contract> GetAllContract()
        {
            List<Contract> output = new List<Contract>();

            this.contractMgmtLogic.GetAllContracts().ToList().ForEach(x => output.Add(new Contract() { PlayerId = x.PlayerId, ClubId = x.ClubId, ReleaseClause = x.ReleaseClause, ContractDate = x.ContractDate, ContractYears = x.ContractYears, Agent = x.Agent, Salary = x.Salary }));

            return output;
        }

        /// <summary>
        /// Add a club.
        /// </summary>
        /// <param name="list">The list of the clubs.</param>
        public void AddClub(IList<Club> list)
        {
            Club newClub = new Club();
            if (list != null && this.editorservice.EditClub(newClub) == true)
            {
                this.clubLeaderLogic.AddClub(newClub.Name, newClub.Coach, newClub.Founded, newClub.Country, newClub.ChampionshipWins);

                var clublist = this.clubLeaderLogic.GetAllClubs().ToList();
                int id = clublist[clublist.Count - 1].ClubId;

                newClub.ClubId = id;

                list?.Add(newClub);

                this.messengerService.Send("ADD OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("ADD CANCEL", "LogicResult");
            }
        }

        /// <summary>
        /// Add a contract.
        /// </summary>
        /// <param name="list">The list of the contracts.</param>
        public void AddContract(IList<Contract> list)
        {
            Contract newContract = new Contract();
            if (list != null && this.editorservice.EditContract(newContract) == true)
            {
                this.contractMgmtLogic.AddContract(newContract.PlayerId, newContract.ClubId, newContract.ContractYears, newContract.ContractDate, newContract.Salary, newContract.ReleaseClause, newContract.Agent);

                var contractlist = this.contractMgmtLogic.GetAllContracts().ToList();
                int pid = contractlist[contractlist.Count - 1].PlayerId;
                int cid = contractlist[contractlist.Count - 1].ClubId;

                newContract.PlayerId = pid;
                newContract.ClubId = cid;

                list?.Add(newContract);

                this.messengerService.Send("ADD OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("ADD CANCEL", "LogicResult");
            }
        }

        /// <summary>
        /// Add a player.
        /// </summary>
        /// <param name="list">The list of the players.</param>
        public void AddPlayer(IList<Player> list)
        {
            Player newPlayer = new Player();
            if (list != null && this.editorservice.EditPlayer(newPlayer) == true)
            {
                this.contractMgmtLogic.AddPlayer(newPlayer.Name, newPlayer.Age, newPlayer.Goals, newPlayer.Nation, newPlayer.Position.ToString());

                var playerlist = this.contractMgmtLogic.GetAllPlayers().ToList();
                int id = playerlist[playerlist.Count - 1].PlayerId;

                newPlayer.PlayerId = id;

                list?.Add(newPlayer);

                this.messengerService.Send("ADD OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("ADD CANCEL", "LogicResult");
            }
        }

        /// <summary>
        /// Delete a player.
        /// </summary>
        /// <param name="list">The list of the clubs.</param>
        /// <param name="club">The club we want to delete.</param>
        public void DelClub(IList<Club> list, Club club)
        {
            if (club != null && list != null && list.Remove(club))
            {
                this.clubLeaderLogic.Delete(club.ClubId);
                this.messengerService.Send("DELETE OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("DELETE FAILED", "LogicResult");
            }
        }

        /// <summary>
        /// Delete a player.
        /// </summary>
        /// <param name="list">The list of the contracts.</param>
        /// <param name="contract">The contract we want to delete.</param>
        public void DelContract(IList<Contract> list, Contract contract)
        {
            if (contract != null && list != null && list.Remove(contract))
            {
                this.contractMgmtLogic.Delete(contract.PlayerId, contract.ClubId);
                this.messengerService.Send("DELETE OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("DELETE FAILED", "LogicResult");
            }
        }

        /// <summary>
        /// Delete a player.
        /// </summary>
        /// <param name="list">The list of the players.</param>
        /// <param name="player">The player we want to delete.</param>
        public void DelPlayer(IList<Player> list, Player player)
        {
            if (player != null && list != null && list.Remove(player))
            {
                this.contractMgmtLogic.Delete(player.PlayerId);
                this.messengerService.Send("DELETE OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("DELETE FAILED", "LogicResult");
            }
        }

        /// <summary>
        /// Modify a club.
        /// </summary>
        /// <param name="clubtoModify">The club we want to modify.</param>
        public void ModClub(Club clubtoModify)
        {
            if (clubtoModify == null)
            {
                this.messengerService.Send("EDIT FAILED", "LogicResult");
                return;
            }

            Club clone = new Club();
            clone.CopyFrom(clubtoModify);
            if (this.editorservice.EditClub(clone) == true)
            {
                clubtoModify.CopyFrom(clone);

                this.clubLeaderLogic.ChangeName(clubtoModify.ClubId, clubtoModify.Name);
                this.clubLeaderLogic.ChangeFounded(clubtoModify.ClubId, clubtoModify.Founded);
                this.clubLeaderLogic.ChangeCountry(clubtoModify.ClubId, clubtoModify.Country);
                this.clubLeaderLogic.ChangeCoach(clubtoModify.ClubId, clubtoModify.Coach);
                this.clubLeaderLogic.ChangeChampionshipWins(clubtoModify.ClubId, clubtoModify.ChampionshipWins);
                this.messengerService.Send("MODIFY OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("MODIFY CANCEL", "LogicResult");
            }
        }

        /// <summary>
        /// Modify a contract.
        /// </summary>
        /// <param name="contracttoModify">The contract we want to modify.</param>
        public void ModContract(Contract contracttoModify)
        {
            if (contracttoModify == null)
            {
                this.messengerService.Send("EDIT FAILED", "LogicResult");
                return;
            }

            Contract clone = new Contract();
            clone.CopyFrom(contracttoModify);
            if (this.editorservice.EditContract(clone) == true)
            {
                contracttoModify.CopyFrom(clone);

                this.contractMgmtLogic.ChangeAgent(contracttoModify.PlayerId, contracttoModify.ClubId, contracttoModify.Agent);
                this.contractMgmtLogic.ChangeContractDate(contracttoModify.PlayerId, contracttoModify.ClubId, contracttoModify.ContractDate);
                this.contractMgmtLogic.ChangeContractYears(contracttoModify.PlayerId, contracttoModify.ClubId, contracttoModify.ContractYears);
                this.contractMgmtLogic.ChangeReleaseClause(contracttoModify.PlayerId, contracttoModify.ClubId, contracttoModify.ReleaseClause);
                this.contractMgmtLogic.ChangeSalary(contracttoModify.PlayerId, contracttoModify.ClubId, contracttoModify.Salary);
                this.messengerService.Send("MODIFY OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("MODIFY CANCEL", "LogicResult");
            }
        }

        /// <summary>
        /// Modify a player.
        /// </summary>
        /// <param name="playertoModify">The player we want to modify.</param>
        public void ModPlayer(Player playertoModify)
        {
            if (playertoModify == null)
            {
                this.messengerService.Send("EDIT FAILED", "LogicResult");
                return;
            }

            Player clone = new Player();
            clone.CopyFrom(playertoModify);
            if (this.editorservice.EditPlayer(clone) == true)
            {
                playertoModify.CopyFrom(clone);

                this.contractMgmtLogic.ChangeAge(playertoModify.PlayerId, playertoModify.Age);
                this.contractMgmtLogic.ChangeGoals(playertoModify.PlayerId, playertoModify.Goals);
                this.contractMgmtLogic.ChangeName(playertoModify.PlayerId, playertoModify.Name);
                this.contractMgmtLogic.ChangeNation(playertoModify.PlayerId, playertoModify.Nation);
                this.contractMgmtLogic.ChangePosition(playertoModify.PlayerId, playertoModify.Position.ToString());
                this.messengerService.Send("MODIFY OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("MODIFY CANCEL", "LogicResult");
            }
        }
    }
}
