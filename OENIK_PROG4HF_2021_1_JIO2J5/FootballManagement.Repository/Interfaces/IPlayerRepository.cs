﻿// <copyright file="IPlayerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using FootballManagement.Data;

    /// <summary>
    /// IPlayerRepository.
    /// </summary>
    public interface IPlayerRepository : IFootballRepository<Player>
    {
        /// <summary>
        /// Change the name of the player.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newName">New name of the player.</param>
        void ChangeName(int id, string newName);

        /// <summary>
        /// Change player Age.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newAge">New Age of the player.</param>
        void ChangeAge(int id, int newAge);

        /// <summary>
        /// Change the number of Goals.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newGoals">New number of Goals.</param>
        void ChangeGoals(int id, int newGoals);

        /// <summary>
        /// Change the Nation of the player.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newNation">New Nation of the player.</param>
        void ChangeNation(int id, string newNation);

        /// <summary>
        /// Change the player Position.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newPosition">New player Position.</param>
        void ChangePosition(int id, string newPosition);

        /// <summary>
        /// Delete a row.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        void Delete(int id);

        /// <summary>
        /// Get Player By Id.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <returns>Returns the player with the given id.</returns>
        Player GetOne(int id);

        /// <summary>
        /// Add a new Player.
        /// </summary>
        /// <param name="name">Name of the palyer.</param>
        /// <param name="age">Player age.</param>
        /// <param name="goals">Number of goals.</param>
        /// <param name="nation">Nation of the player.</param>
        /// <param name="position">Player Position on pitch.</param>
        void AddPlayer(string name, int age, int goals, string nation, string position);
    }
}
