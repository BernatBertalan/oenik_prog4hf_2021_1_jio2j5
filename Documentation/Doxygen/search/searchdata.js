var indexSectionsWithContent =
{
  0: "acdefghilmnoprstvx",
  1: "acefghimpstv",
  2: "afx",
  3: "acdefghilmoprst",
  4: "p",
  5: "clr",
  6: "acdefgilmnoprs",
  7: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "enums",
  5: "enumvalues",
  6: "properties",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Enumerations",
  5: "Enumerator",
  6: "Properties",
  7: "Pages"
};

