﻿// <copyright file="Salaries.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Logic.ReturnClasses
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Salaries class for NON-CRUDS.
    /// </summary>
    public class Salaries
    {
        /// <summary>
        /// Gets or sets Name of the Player.
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        ///  Gets or sets the salary of the player.
        /// </summary>
        public int Salary { get; set; }

        /// <summary>
        /// Overrided toString, to show noncrud.
        /// </summary>
        /// <returns>Formed string.</returns>
        public override string ToString()
        {
            return $"{this.PlayerName} - Salary: {this.Salary}";
        }

        /// <summary>
        /// Overrided Equals.
        /// </summary>
        /// <param name="obj">An object.</param>
        /// <returns>Returns true or false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Salaries)
            {
                Salaries other = obj as Salaries;
                return this.Salary == other.Salary &&
                    this.PlayerName == other.PlayerName;
            }

            return false;
        }

        /// <summary>
        /// Overrided GetHashCode.
        /// </summary>
        /// <returns>Obect hashcode.</returns>
        public override int GetHashCode()
        {
            return this.PlayerName.GetHashCode(System.StringComparison.CurrentCulture) + (int)this.Salary;
        }
    }
}