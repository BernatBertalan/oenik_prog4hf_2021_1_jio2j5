﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// MainViewModel implements ViewModelBase class.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private IMainLogic logic;
        private PlayerVM selectedPlayer;
        private ObservableCollection<PlayerVM> allPlayers;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">IMainLogic interface.</param>
        public MainViewModel(IMainLogic logic)
        {
            this.logic = logic;

            this.LoadCmd = new RelayCommand(() => this.AllPlayers = new ObservableCollection<PlayerVM>(this.logic.ApiGetPlayers()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelPlayer(this.selectedPlayer));
            this.AddCmd = new RelayCommand(() => this.logic.EditPlayer(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditPlayer(this.selectedPlayer, this.EditorFunc));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IMainLogic>())
        {
        }

        /// <summary>
        /// Gets or sets observable collection of players.
        /// </summary>
        public ObservableCollection<PlayerVM> AllPlayers
        {
            get { return this.allPlayers; }
            set { this.Set(ref this.allPlayers, value); }
        }

        /// <summary>
        /// Gets or sets actual selected player.
        /// </summary>
        public PlayerVM SelectedPlayer
        {
            get { return this.selectedPlayer; }
            set { this.Set(ref this.selectedPlayer, value); }
        }

        /// <summary>
        /// Gets or sets will be implement in MainWindow.
        /// </summary>
        public Func<PlayerVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets add a player command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets modify a player command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets delete a player command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets Load a player command.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
