﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Player model class.
    /// </summary>
    public class Player
    {
        /// <summary>
        /// Gets or Sets PlayerId.
        /// </summary>
        [Display(Name = "Player ID")]
        [Required]
        public int PlayerId { get; set; }

        /// <summary>
        /// Gets or Sets Name of the player.
        /// </summary>
        [Display(Name = "Player Name")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets Age of the player.
        /// </summary>
        [Display(Name = "Player Age")]
        [Required]
        public int Age { get; set; }

        /// <summary>
        /// Gets or Sets number of Goals.
        /// </summary>
        [Display(Name = "Number of Goals")]
        [Required]
        public int Goals { get; set; }

        /// <summary>
        /// Gets or Sets Nation of the player.
        /// </summary>
        [Display(Name = "Player Nation")]
        [Required]
        public string Nation { get; set; }

        /// <summary>
        /// Gets or Sets Position on the pitch.
        /// </summary>
        [Display(Name = "Player Position")]
        [Required]
        public string Position { get; set; }
    }
}
