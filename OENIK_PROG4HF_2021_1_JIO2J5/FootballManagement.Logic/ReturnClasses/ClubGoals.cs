﻿// <copyright file="ClubGoals.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Logic.ReturnClasses
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// ClubGoals class for NON-CRUDS.
    /// </summary>
    public class ClubGoals
    {
        /// <summary>
        /// Gets or sets Name of club.
        /// </summary>
        public string ClubName { get; set; }

        /// <summary>
        ///  Gets or sets Name of club Number of goals.
        /// </summary>
        public int Goals { get; set; }

        /// <summary>
        /// Overrided toString, to show noncrud.
        /// </summary>
        /// <returns>Formed string.</returns>
        public override string ToString()
        {
            return $"{this.ClubName} - Goals: {this.Goals}";
        }

        /// <summary>
        /// Overrided Equals.
        /// </summary>
        /// <param name="obj">An object.</param>
        /// <returns>Returns true or false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is ClubGoals)
            {
                ClubGoals other = obj as ClubGoals;
                return this.Goals == other.Goals &&
                    this.ClubName == other.ClubName;
            }

            return false;
        }

        /// <summary>
        /// Overrided GetHashCode.
        /// </summary>
        /// <returns>Obect hashcode.</returns>
        public override int GetHashCode()
        {
            return this.ClubName.GetHashCode(System.StringComparison.CurrentCulture) + (int)this.Goals;
        }
    }
}
