﻿// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Program.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballManagement.Program.Data;

    /// <summary>
    /// EditorService interface.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Editing OK or CANCEL.
        /// </summary>
        /// <param name="p">A Player.</param>
        /// <returns>True or false.</returns>
        bool EditPlayer(Player p);

        /// <summary>
        /// Editing OK or CANCEL.
        /// </summary>
        /// <param name="p">A Player.</param>
        /// <returns>True or false.</returns>
        bool EditClub(Club p);

        /// <summary>
        /// Editing OK or CANCEL.
        /// </summary>
        /// <param name="p">A Player.</param>
        /// <returns>True or false.</returns>
        bool EditContract(Contract p);
    }
}
