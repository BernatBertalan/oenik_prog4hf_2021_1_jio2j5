var interface_football_management_1_1_logic_1_1_i_contract_management_logic =
[
    [ "AddContract", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#a95e7f178d1ee072e4525a633dbca5a38", null ],
    [ "AddPlayer", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#ad6c9a7cd41e118d5f5183fda0f266c63", null ],
    [ "ChangeAge", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#a9f26643381fd88ccf05e73f05197493a", null ],
    [ "ChangeAgent", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#a0adeb2e3690dc181413fd1eb2a7103a7", null ],
    [ "ChangeContractDate", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#a6247a66c7ed064a7a4bd8899dc7a5cc8", null ],
    [ "ChangeContractYears", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#a9e335ad9ed451f616dc85561bec95ffe", null ],
    [ "ChangeGoals", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#acd8ad17809b8c627aa4384ff07449c2d", null ],
    [ "ChangeName", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#ab3137798b33ea65b8f28957fb943609a", null ],
    [ "ChangeNation", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#aac548b445c3cf154944ecd09f06a5b60", null ],
    [ "ChangePlayer", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#a1bd834f531361bc4c4a82529dd73bd86", null ],
    [ "ChangePosition", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#a39a3faa255ab74bbfe745d8b86f58154", null ],
    [ "ChangeReleaseClause", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#a3afd8d5869ccf67972bf7dd673d78438", null ],
    [ "ChangeSalary", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#a654f3de82abd8154e50a3ed8c5189b94", null ],
    [ "Delete", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#af286e8505dd998cb5cb2c727a3a3a626", null ],
    [ "Delete", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#a33d47174dd764127085b2b0769264e09", null ],
    [ "DeletePlayer", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#a655b7b10a6440f7a3a03bd2caa26ff27", null ],
    [ "GetAllContracts", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#a69d043a0de3f07622ef7e6b5cce941d6", null ],
    [ "GetAllPlayers", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#a714492599f8dd62ea2e62fe8a988cb12", null ],
    [ "GetContractById", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#a11a148ef733dc45d97df4b29f7c1dee7", null ],
    [ "GetPlayerById", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html#a90d740010b0fd0c7084ce353c1085ac7", null ]
];