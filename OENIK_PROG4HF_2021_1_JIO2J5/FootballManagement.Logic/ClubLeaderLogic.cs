﻿// <copyright file="ClubLeaderLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballManagement.Data;
    using FootballManagement.Logic.ReturnClasses;
    using FootballManagement.Repository.Interfaces;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Related to club leadership.
    /// </summary>
    public class ClubLeaderLogic : IClubLeaderLogic
    {
        private IClubRepository clubRepo;
        private IPlayerRepository playerRepo;
        private IContractRepository contractRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClubLeaderLogic"/> class.
        /// </summary>
        /// <param name="repo">Club Repository.</param>
        /// <param name="crepo">Contract Repository.</param>
        /// <param name="prepo">Player Repository.</param>
        public ClubLeaderLogic(IClubRepository repo, IContractRepository crepo, IPlayerRepository prepo)
        {
            this.clubRepo = repo;
            this.playerRepo = prepo;
            this.contractRepo = crepo;
        }

        /// <summary>
        /// Add a new Club.
        /// </summary>
        /// <param name="name">Nameof the Club.</param>
        /// <param name="coach">Name of the coach.</param>
        /// <param name="founded">When was it founded.</param>
        /// <param name="country">Country of the club.</param>
        /// <param name="championshipWins">Number of Championship wins.</param>
        public void AddClub(string name, string coach, int founded, string country, int championshipWins)
        {
            this.clubRepo.AddClub(name, coach, founded, country, championshipWins);
        }

        /// <summary>
        /// Change number of Championship wins.
        /// </summary>
        /// <param name="id">Searched ClubId.</param>
        /// <param name="newWins">New number of Wins.</param>
        public void ChangeChampionshipWins(int id, int newWins)
        {
            this.clubRepo.ChangeChampionshipWins(id, newWins);
        }

        /// <summary>
        /// Change Coach of the club.
        /// </summary>
        /// <param name="id">Searched clubId.</param>
        /// <param name="newCoach">New Coach of the club.</param>
        public void ChangeCoach(int id, string newCoach)
        {
            this.clubRepo.ChangeCoach(id, newCoach);
        }

        /// <summary>
        /// Change Country of the club.
        /// </summary>
        /// <param name="id">Searched clubId.</param>
        /// <param name="newCountry">New Country of the club.</param>
        public void ChangeCountry(int id, string newCountry)
        {
            this.clubRepo.ChangeCountry(id, newCountry);
        }

        /// <summary>
        /// Change when was the club Founded.
        /// </summary>
        /// <param name="id">Searched clubId.</param>
        /// <param name="newFounded">New Founded year.</param>
        public void ChangeFounded(int id, int newFounded)
        {
            this.clubRepo.ChangeFounded(id, newFounded);
        }

        /// <summary>
        /// Change name of the club.
        /// </summary>
        /// <param name="id">Searched clubId.</param>
        /// <param name="newName">New name of the club.</param>
        public void ChangeName(int id, string newName)
        {
            this.clubRepo.ChangeName(id, newName);
        }

        /// <summary>
        /// Delete row.
        /// </summary>
        /// <param name="id">ClubId to delete.</param>
        public void Delete(int id)
        {
            this.clubRepo.Delete(id);
        }

        /// <summary>
        /// Get All Clubs.
        /// </summary>
        /// <returns>Returns all the clubs.</returns>
        public IList<Club> GetAllClubs()
        {
            return this.clubRepo.GetAll().ToList();
        }

        /// <summary>
        /// Get Club By Id.
        /// </summary>
        /// <param name="id">Searched ClubId.</param>
        /// <returns>The club with given id.</returns>
        public Club GetClubById(int id)
        {
            return this.clubRepo.GetOne(id);
        }

        /// <summary>
        /// NONCRUD to list clubs and their goals.
        /// </summary>
        /// <returns>Returns a Club name and the number of goals.</returns>
        public IList<ClubGoals> GoalsPerClubs()
        {
            var q = from p in this.playerRepo.GetAll()
                     join c in this.contractRepo.GetAll() on p.PlayerId equals c.PlayerId
                     join k in this.clubRepo.GetAll() on c.ClubId equals k.ClubId
                     group p by k.Name into g
                     orderby g.Sum(x => x.Goals) descending
                     select new ClubGoals
                     {
                         ClubName = g.Key,
                         Goals = g.Sum(x => x.Goals),
                     };
            return q.ToList();
        }

        /// <summary>
        /// NONCRUD to show the most effective club.
        /// </summary>
        /// <returns>A club with the highest championship wins.</returns>
        public IList<MostWins> MostChampionshipWins()
        {
            var q = (from c in this.clubRepo.GetAll()
                     orderby c.ChampionshipWins descending
                     select new MostWins
                     {
                        ClubName = c.Name,
                        ChampionshipWins = c.ChampionshipWins,
                     }).Take(1);
            return q.ToList();
        }

        /// <summary>
        /// Async version of NONCRUD to list clubs and their goals.
        /// </summary>
        /// <returns>Returns a Club name and the number of goals.</returns>
        public async Task<IList<ClubGoals>> GoalsPerClubsAsync()
        {
            var q = from p in this.playerRepo.GetAll()
                    join c in this.contractRepo.GetAll() on p.PlayerId equals c.PlayerId
                    join k in this.clubRepo.GetAll() on c.ClubId equals k.ClubId
                    group p by k.Name into g
                    orderby g.Sum(x => x.Goals) descending
                    select new ClubGoals
                    {
                        ClubName = g.Key,
                        Goals = g.Sum(x => x.Goals),
                    };
            return await q.ToListAsync().ConfigureAwait(false);
        }

        /// <summary>
        /// Async version of NONCRUD to show the most effective club..
        /// </summary>
        /// <returns>A club with the highest championship wins.</returns>
        public async Task<IList<MostWins>> MostChampionshipWinsAsync()
        {
            var q = (from c in this.clubRepo.GetAll()
                     orderby c.ChampionshipWins descending
                     select new MostWins
                     {
                         ClubName = c.Name,
                         ChampionshipWins = c.ChampionshipWins,
                     }).Take(1);
            return await q.ToListAsync().ConfigureAwait(false);
        }
    }
}
