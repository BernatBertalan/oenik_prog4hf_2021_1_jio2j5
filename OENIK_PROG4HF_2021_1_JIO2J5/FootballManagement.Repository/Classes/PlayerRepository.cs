﻿// <copyright file="PlayerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballManagement.Data;
    using FootballManagement.Repository.Interfaces;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// PlayerRepository.
    /// </summary>
    public class PlayerRepository : FootballRepository<Player>, IPlayerRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlayerRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext ctx.</param>
        public PlayerRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Change player Age.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newAge">New Age of the player.</param>
        public void ChangeAge(int id, int newAge)
        {
            if (this.GetOne(id) != null)
            {
                var player = this.GetOne(id);
                player.Age = newAge;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This ID is not exist!");
            }
        }

        /// <summary>
        /// Change the number of Goals.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newGoals">New number of Goals.</param>
        public void ChangeGoals(int id, int newGoals)
        {
            if (this.GetOne(id) != null)
            {
                var player = this.GetOne(id);
                player.Goals = newGoals;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This ID is not exist!");
            }
        }

        /// <summary>
        /// Change the name of the player.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newName">New name of the player.</param>
        public void ChangeName(int id, string newName)
        {
            if (this.GetOne(id) != null)
            {
                var player = this.GetOne(id);
                player.Name = newName;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This ID is not exist!");
            }
        }

        /// <summary>
        /// Change the Nation of the player.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newNation">New Nation of the player.</param>
        public void ChangeNation(int id, string newNation)
        {
            if (this.GetOne(id) != null)
            {
                var player = this.GetOne(id);
                player.Nation = newNation;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This ID is not exist!");
            }
        }

        /// <summary>
        /// Change the player Position.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newPosition">New player Position.</param>
        public void ChangePosition(int id, string newPosition)
        {
            if (this.GetOne(id) != null)
            {
                var player = this.GetOne(id);
                player.Position = newPosition;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This ID is not exist!");
            }
        }

        /// <summary>
        /// Delete a row.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        public void Delete(int id)
        {
            if (this.GetOne(id) != null)
            {
                var player = this.GetOne(id);
                this.Ctx.Remove(player);
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This ID is not exist!");
            }
        }

        /// <summary>
        /// Get Player By Id.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <returns>Returns the player with the given id.</returns>
        public Player GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.PlayerId == id);
        }

        /// <summary>
        /// Add a new Player.
        /// </summary>
        /// <param name="name">Name of the palyer.</param>
        /// <param name="age">Player age.</param>
        /// <param name="goals">Number of goals.</param>
        /// <param name="nation">Nation of the player.</param>
        /// <param name="position">Player Position on pitch.</param>
        public void AddPlayer(string name, int age, int goals, string nation, string position)
        {
            this.Ctx.Add(new Player() { Name = name, Age = age, Goals = goals, Nation = nation, Position = position });
            this.Ctx.SaveChanges();
        }
    }
}
