﻿// <copyright file="ContractManagementLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballManagement.Data;
    using FootballManagement.Logic.ReturnClasses;
    using FootballManagement.Repository.Interfaces;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Related to the player and his contracts.
    /// </summary>
    public class ContractManagementLogic : IContractManagementLogic
    {
        private IContractRepository contractRepo;
        private IPlayerRepository playerRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractManagementLogic"/> class.
        /// </summary>
        /// <param name="crepo">IContractRepository.</param>
        /// <param name="prepo">IPlayerRepository.</param>
        public ContractManagementLogic(IContractRepository crepo, IPlayerRepository prepo)
        {
            this.playerRepo = prepo;
            this.contractRepo = crepo;
        }

        /// <summary>
        /// Add a new Player.
        /// </summary>
        /// <param name="name">Name of the palyer.</param>
        /// <param name="age">Player age.</param>
        /// <param name="goals">Number of goals.</param>
        /// <param name="nation">Nation of the player.</param>
        /// <param name="position">Player Position on pitch.</param>
        public void AddPlayer(string name, int age, int goals, string nation, string position)
        {
            this.playerRepo.AddPlayer(name, age, goals, nation, position);
        }

        /// <summary>
        /// Change player Age.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newAge">New Age of the player.</param>
        public void ChangeAge(int id, int newAge)
        {
            this.playerRepo.ChangeAge(id, newAge);
        }

        /// <summary>
        /// Change the number of Goals.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newGoals">New number of Goals.</param>
        public void ChangeGoals(int id, int newGoals)
        {
            this.playerRepo.ChangeGoals(id, newGoals);
        }

        /// <summary>
        /// Change the name of the player.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newName">New name of the player.</param>
        public void ChangeName(int id, string newName)
        {
            this.playerRepo.ChangeName(id, newName);
        }

        /// <summary>
        /// Change the Nation of the player.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newNation">New Nation of the player.</param>
        public void ChangeNation(int id, string newNation)
        {
            this.playerRepo.ChangeNation(id, newNation);
        }

        /// <summary>
        /// Change the player Position.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newPosition">New player Position.</param>
        public void ChangePosition(int id, string newPosition)
        {
            this.playerRepo.ChangePosition(id, newPosition);
        }

        /// <summary>
        /// Change player's all details.
        /// </summary>
        /// <param name="id">Id of the player.</param>
        /// <param name="newName">Name of the player.</param>
        /// <param name="newAge">Age of the player.</param>
        /// <param name="newNation">Nation of the player.</param>
        /// <param name="newGoals">Number of goals.</param>
        /// <param name="newPosition">Position of the player.</param>
        /// <returns>True or false.</returns>
        public bool ChangePlayer(int id, string newName, int newAge, string newNation, int newGoals, string newPosition)
        {
            try
            {
                this.playerRepo.ChangeName(id, newName);
                this.playerRepo.ChangeAge(id, newAge);
                this.playerRepo.ChangeNation(id, newNation);
                this.playerRepo.ChangeGoals(id, newGoals);
                this.playerRepo.ChangePosition(id, newPosition);
            }
            catch (NullReferenceException)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Delete a row.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        public void Delete(int id)
        {
            this.playerRepo.Delete(id);
        }

        /// <summary>
        /// Delete a row  for web app, bc we need to return a bool. (If I modify the real Delete method the WPF delete wont work).
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <returns>True or False.</returns>
        public bool DeletePlayer(int id)
        {
            try
            {
                this.playerRepo.Delete(id);
            }
            catch (NullReferenceException)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get All Players.
        /// </summary>
        /// <returns>Returns all the players.</returns>
        public IList<Player> GetAllPlayers()
        {
            return this.playerRepo.GetAll().ToList();
        }

        /// <summary>
        /// Get Player By Id.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <returns>Returns the player with the given id.</returns>
        public Player GetPlayerById(int id)
        {
            return this.playerRepo.GetOne(id);
        }

        /// <summary>
        /// Add a new Contract.
        /// </summary>
        /// <param name="playerid">Id of the player you wants to add.</param>
        /// <param name="clubid">Id of the clubs who signed the player.</param>
        /// <param name="contractyears">Contract years.</param>
        /// <param name="contractdate">Date of the contract.</param>
        /// <param name="salary">Salary in euro.</param>
        /// <param name="releaseclause">Release Clause in euro.</param>
        /// <param name="agent">Player Agent.</param>
        public void AddContract(int playerid, int clubid, int contractyears, string contractdate, int salary, int releaseclause, string agent)
        {
            this.contractRepo.AddContract(playerid, clubid, contractyears, contractdate, salary, releaseclause, agent);
        }

        /// <summary>
        /// Change the player agent.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newAgent">New player agent.</param>
        public void ChangeAgent(int playerid, int clubid, string newAgent)
        {
            this.contractRepo.ChangeAgent(playerid, clubid, newAgent);
        }

        /// <summary>
        /// Change date of contract.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newContractDate">New date of contract.</param>
        public void ChangeContractDate(int playerid, int clubid, string newContractDate)
        {
            this.contractRepo.ChangeContractDate(playerid, clubid, newContractDate);
        }

        /// <summary>
        /// Change number of Contract years.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newContractYears">New number of Contract years.</param>
        public void ChangeContractYears(int playerid, int clubid, int newContractYears)
        {
            this.contractRepo.ChangeContractYears(playerid, clubid, newContractYears);
        }

        /// <summary>
        /// Change the Release clause.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newReleaseClause">New Release clause of the player.</param>
        public void ChangeReleaseClause(int playerid, int clubid, int newReleaseClause)
        {
            this.contractRepo.ChangeReleaseClause(playerid, clubid, newReleaseClause);
        }

        /// <summary>
        /// Change the Salary of the player.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newSalary">New value of Salary.</param>
        public void ChangeSalary(int playerid, int clubid, int newSalary)
        {
            this.contractRepo.ChangeSalary(playerid, clubid, newSalary);
        }

        /// <summary>
        /// Delete a row.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        public void Delete(int playerid, int clubid)
        {
            this.contractRepo.Delete(playerid, clubid);
        }

        /// <summary>
        /// Get All Contracts.
        /// </summary>
        /// <returns>Returns all the Contracts.</returns>
        public IList<Contract> GetAllContracts()
        {
            return this.contractRepo.GetAll().ToList();
        }

        /// <summary>
        /// Get Contract By Id.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <returns>Returns the contract with the given id's.</returns>
        public Contract GetContractById(int playerid, int clubid)
        {
            return this.contractRepo.GetOne(playerid, clubid);
        }

        /// <summary>
        /// NONCRUD to list contracts with less then two years contract years.
        /// </summary>
        /// <returns>Returns the players name, age, contract date and contract years if the contract years less than 2.</returns>
        public IList<PlayerWithContract> LessThanTwoYearContract()
        {
            var q = from p in this.playerRepo.GetAll()
                     join c in this.contractRepo.GetAll() on p.PlayerId equals c.PlayerId
                     where c.ContractYears < 2
                     select new PlayerWithContract
                     {
                         PlayerName = p.Name,
                         Age = p.Age,
                         ContractDate = c.ContractDate,
                         ContractYears = c.ContractYears,
                     };
            return q.ToList();
        }

        /// <summary>
        /// NONCRUD to list highest paid players over 30.
        /// </summary>
        /// <returns>Player name and a salary of the top5 highest paid player.</returns>
        public IList<Salaries> TopFiveHighestSalariesOver30()
        {
            var q = (from p in this.playerRepo.GetAll()
                     join c in this.contractRepo.GetAll() on p.PlayerId equals c.PlayerId
                     where p.Age > 30
                     orderby c.Salary descending
                     select new Salaries
                     {
                         PlayerName = p.Name,
                         Salary = c.Salary,
                     }).Take(5);
            return q.ToList();
        }

        /// <summary>
        /// Async version of NONCRUD to list contracts with less then two years contract years.
        /// </summary>
        /// <returns>Returns the players name, age, contract date and contract years if the contract years less than 2.</returns>
        public async Task<IList<PlayerWithContract>> LessThanTwoYearContractAsync()
        {
            var q = from p in this.playerRepo.GetAll()
                    join c in this.contractRepo.GetAll() on p.PlayerId equals c.PlayerId
                    where c.ContractYears < 2
                    select new PlayerWithContract
                    {
                        PlayerName = p.Name,
                        Age = p.Age,
                        ContractDate = c.ContractDate,
                        ContractYears = c.ContractYears,
                    };
            return await q.ToListAsync().ConfigureAwait(false);
        }

        /// <summary>
        /// Async version of NONCRUD to list highest paid players over 30.
        /// </summary>
        /// <returns>Player name and a salary of the top5 highest paid player.</returns>
        public async Task<IList<Salaries>> TopFiveHighestSalariesOver30Async()
        {
            var q = (from p in this.playerRepo.GetAll()
                     join c in this.contractRepo.GetAll() on p.PlayerId equals c.PlayerId
                     where p.Age > 30
                     orderby c.Salary descending
                     select new Salaries
                     {
                         PlayerName = p.Name,
                         Salary = c.Salary,
                     }).Take(5);
            return await q.ToListAsync().ConfigureAwait(false);
        }
    }
}
