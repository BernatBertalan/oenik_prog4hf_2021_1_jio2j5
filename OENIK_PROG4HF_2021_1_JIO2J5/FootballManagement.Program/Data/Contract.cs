﻿// <copyright file="Contract.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Program.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Contract models.
    /// </summary>
    public class Contract : ObservableObject
    {
        private int clubid;
        private int playerid;
        private int contractyears;
        private int releaseclause;
        private int salary;
        private string contractdate;
        private string agent;

        /// <summary>
        /// Gets or Sets ClubId, references clubs table.
        /// </summary>
        public int ClubId
        {
            get { return this.clubid; }
            set { this.Set(ref this.clubid, value); }
        }

        /// <summary>
        /// Gets or Sets PlayerId, references players table.
        /// </summary>
        public int PlayerId
        {
            get { return this.playerid; }
            set { this.Set(ref this.playerid, value); }
        }

        /// <summary>
        /// Gets or Sets date of contract.
        /// </summary>
        public string ContractDate
        {
            get { return this.contractdate; }
            set { this.Set(ref this.contractdate, value); }
        }

        /// <summary>
        /// Gets or Sets number of Contract years.
        /// </summary>
        public int ContractYears
        {
            get { return this.contractyears; }
            set { this.Set(ref this.contractyears, value); }
        }

        /// <summary>
        /// Gets or Sets salary in Euro.
        /// </summary>
        public int Salary
        {
            get { return this.salary; }
            set { this.Set(ref this.salary, value); }
        }

        /// <summary>
        /// Gets or Sets releaseClause in Euro.
        /// </summary>]
        public int ReleaseClause
        {
            get { return this.releaseclause; }
            set { this.Set(ref this.releaseclause, value); }
        }

        /// <summary>
        /// Gets or Sets the player agent.
        /// </summary>
        public string Agent
        {
            get { return this.agent; }
            set { this.Set(ref this.agent, value); }
        }

        /// <summary>
        /// CopyFrom method.
        /// </summary>
        /// <param name="other"> Other Contract that we want to copy.</param>
        public void CopyFrom(Contract other)
        {
            if (other != null)
            {
                this.Agent = other.Agent;
                this.PlayerId = other.PlayerId;
                this.ClubId = other.ClubId;
                this.ReleaseClause = other.ReleaseClause;
                this.Salary = other.Salary;
                this.ContractYears = other.ContractYears;
                this.ContractDate = other.ContractDate;
            }
        }
    }
}
