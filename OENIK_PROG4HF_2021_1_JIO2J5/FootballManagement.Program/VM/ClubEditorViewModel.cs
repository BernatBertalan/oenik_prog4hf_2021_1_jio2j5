﻿// <copyright file="ClubEditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Program.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballManagement.Program.Data;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Implements ViewModelBase class.
    /// </summary>
    public class ClubEditorViewModel : ViewModelBase
    {
        private Club club;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClubEditorViewModel"/> class.
        /// </summary>
        public ClubEditorViewModel()
        {
            this.club = new Club();
            if (this.IsInDesignMode)
            {
                this.club.Name = "FC Barcelona";
                this.club.Coach = "Ronald Koeman";
                this.club.Country = "Argentina";
            }
        }

        /// <summary>
        /// Gets or sets a Club to edit.
        /// </summary>
        public Club Club
        {
            get { return this.club; }
            set { this.Set(ref this.club, value); }
        }
    }
}
