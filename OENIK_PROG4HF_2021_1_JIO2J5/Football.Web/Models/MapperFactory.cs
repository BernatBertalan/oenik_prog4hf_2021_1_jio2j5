﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// MapperFactory class.
    /// </summary>
    public class MapperFactory
    {
        /// <summary>
        /// Creates maps.
        /// </summary>
        /// <returns>New MapperConfiguration.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<FootballManagement.Data.Player, Web.Models.Player>().
                ForMember(dest => dest.PlayerId, map => map.MapFrom(src => src.PlayerId)).
                ForMember(dest => dest.Goals, map => map.MapFrom(src => src.Goals)).
                ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name)).
                ForMember(dest => dest.Position, map => map.MapFrom(src => src.Position)).
                ForMember(dest => dest.Nation, map => map.MapFrom(src => src.Nation)).
                ForMember(dest => dest.Age, map => map.MapFrom(src => src.Age));
            });
            return config.CreateMapper();
        }
    }
}
