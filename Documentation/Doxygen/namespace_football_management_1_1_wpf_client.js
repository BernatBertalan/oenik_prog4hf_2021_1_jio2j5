var namespace_football_management_1_1_wpf_client =
[
    [ "App", "class_football_management_1_1_wpf_client_1_1_app.html", "class_football_management_1_1_wpf_client_1_1_app" ],
    [ "EditorWindow", "class_football_management_1_1_wpf_client_1_1_editor_window.html", "class_football_management_1_1_wpf_client_1_1_editor_window" ],
    [ "IMainLogic", "interface_football_management_1_1_wpf_client_1_1_i_main_logic.html", "interface_football_management_1_1_wpf_client_1_1_i_main_logic" ],
    [ "MainLogic", "class_football_management_1_1_wpf_client_1_1_main_logic.html", "class_football_management_1_1_wpf_client_1_1_main_logic" ],
    [ "MainViewModel", "class_football_management_1_1_wpf_client_1_1_main_view_model.html", "class_football_management_1_1_wpf_client_1_1_main_view_model" ],
    [ "MainWindow", "class_football_management_1_1_wpf_client_1_1_main_window.html", "class_football_management_1_1_wpf_client_1_1_main_window" ],
    [ "PlayerVM", "class_football_management_1_1_wpf_client_1_1_player_v_m.html", "class_football_management_1_1_wpf_client_1_1_player_v_m" ]
];