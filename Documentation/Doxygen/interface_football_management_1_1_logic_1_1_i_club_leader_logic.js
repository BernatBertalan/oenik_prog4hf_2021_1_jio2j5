var interface_football_management_1_1_logic_1_1_i_club_leader_logic =
[
    [ "AddClub", "interface_football_management_1_1_logic_1_1_i_club_leader_logic.html#a9ab511c2b8a4b972f99319d7b3b22f78", null ],
    [ "ChangeChampionshipWins", "interface_football_management_1_1_logic_1_1_i_club_leader_logic.html#a10ac51b135064bdc269952027a636f68", null ],
    [ "ChangeCoach", "interface_football_management_1_1_logic_1_1_i_club_leader_logic.html#a03b4c31a56360090fd10720ead53a3ce", null ],
    [ "ChangeCountry", "interface_football_management_1_1_logic_1_1_i_club_leader_logic.html#a693fa6d20795583163fe88f1469e2436", null ],
    [ "ChangeFounded", "interface_football_management_1_1_logic_1_1_i_club_leader_logic.html#aca6f7e1ffb5d427005b9718f0316a3f3", null ],
    [ "ChangeName", "interface_football_management_1_1_logic_1_1_i_club_leader_logic.html#ab19c990b95280ed0706c513ee8329438", null ],
    [ "Delete", "interface_football_management_1_1_logic_1_1_i_club_leader_logic.html#a13856509f3980cdb92eb0257954778c1", null ],
    [ "GetAllClubs", "interface_football_management_1_1_logic_1_1_i_club_leader_logic.html#ad3029a475503b57b151597556d10fae8", null ],
    [ "GetClubById", "interface_football_management_1_1_logic_1_1_i_club_leader_logic.html#a1d6eaa2f1aad243c19b9da5db9320b78", null ]
];