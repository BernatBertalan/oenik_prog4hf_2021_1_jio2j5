﻿// <copyright file="PlayersApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using FootballManagement.Logic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Api Controller class.
    /// </summary>
    public class PlayersApiController : Controller
    {
        private IContractManagementLogic logic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayersApiController"/> class.
        /// </summary>
        /// <param name="logic">IContractManagementLogic logic instance.</param>
        /// <param name="mapper">IMapper instance.</param>
        public PlayersApiController(IContractManagementLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Api version of GetAll.
        /// </summary>
        /// <returns>All the players.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Player> GetAll()
        {
            var players = this.logic.GetAllPlayers();
            return this.mapper.Map<IList<Data.Player>, List<Models.Player>>(players);
        }

        /// <summary>
        /// Api version of delete.
        /// </summary>
        /// <param name="id">Id of the player.</param>
        /// <returns>ApiResult opretation result.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOneCar(int id)
        {
            return new ApiResult() { OperationResult = this.logic.DeletePlayer(id) };
        }

        /// <summary>
        /// Api version of add.
        /// </summary>
        /// <param name="player">Player that we want to add.</param>
        /// <returns>ApiResult opretation result.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOnePlayer(Models.Player player)
        {
            bool success = true;
            try
            {
                this.logic.AddPlayer(player.Name, player.Age, player.Goals, player.Nation, player.Position);
            }
            catch (ArgumentException)
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// Api version of modify.
        /// </summary>
        /// <param name="player">Player that we want to modify.</param>
        /// <returns>ApiResult opretation result.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneCar(Models.Player player)
        {
            return new ApiResult() { OperationResult = this.logic.ChangePlayer(player.PlayerId, player.Name, player.Age, player.Nation, player.Goals, player.Position) };
        }

        /// <summary>
        /// For flexible return.
        /// </summary>
        public class ApiResult
        {
            /// <summary>
            /// Gets or sets a value indicating whether result of add, mod del.
            /// </summary>
            public bool OperationResult { get; set; }
        }
    }
}
