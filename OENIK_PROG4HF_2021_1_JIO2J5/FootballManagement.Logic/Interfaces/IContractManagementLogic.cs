﻿// <copyright file="IContractManagementLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using FootballManagement.Data;

    /// <summary>
    /// IContractLogic interface.
    /// </summary>
    public interface IContractManagementLogic
    {
        /// <summary>
        /// Get Player By Id.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <returns>Returns the player with the given id.</returns>
        Player GetPlayerById(int id);

        /// <summary>
        /// Change the name of the player.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newName">New name of the player.</param>
        void ChangeName(int id, string newName);

        /// <summary>
        /// Change player Age.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newAge">New Age of the player.</param>
        void ChangeAge(int id, int newAge);

        /// <summary>
        /// Change the number of Goals.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newGoals">New number of Goals.</param>
        void ChangeGoals(int id, int newGoals);

        /// <summary>
        /// Change the Nation of the player.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newNation">New Nation of the player.</param>
        void ChangeNation(int id, string newNation);

        /// <summary>
        /// Change the player Position.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// <param name="newPosition">New player Position.</param>
        void ChangePosition(int id, string newPosition);

        /// <summary>
        /// Change player's all details.
        /// </summary>
        /// <param name="id">Id of the player.</param>
        /// <param name="newName">Name of the player.</param>
        /// <param name="newAge">Age of the player.</param>
        /// <param name="newNation">Nation of the player.</param>
        /// <param name="newGoals">Number of goals.</param>
        /// <param name="newPosition">Position of the player.</param>
        /// <returns>True or false.</returns>
        bool ChangePlayer(int id, string newName, int newAge, string newNation, int newGoals, string newPosition);

        /// <summary>
        /// Delete a row.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        void Delete(int id);

        /// <summary>
        /// Delete a row  for web app, bc we need to return a bool.
        /// </summary>
        /// <param name="id">Searched PlayerId.</param>
        /// /// <returns>True or false.</returns>
        bool DeletePlayer(int id);

        /// <summary>
        /// Add a new Player.
        /// </summary>
        /// <param name="name">Name of the palyer.</param>
        /// <param name="age">Player age.</param>
        /// <param name="goals">Number of goals.</param>
        /// <param name="nation">Nation of the player.</param>
        /// <param name="position">Player Position on pitch.</param>
        void AddPlayer(string name, int age, int goals, string nation, string position);

        /// <summary>
        /// Get All Players.
        /// </summary>
        /// <returns>Returns all the players.</returns>
        IList<Player> GetAllPlayers();

        /// <summary>
        /// Get Contract By Id.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <returns>Returns the contract with the given id's.</returns>
        Contract GetContractById(int playerid, int clubid);

        /// <summary>
        /// Change number of Contract years.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newContractYears">New number of Contract years.</param>
        void ChangeContractYears(int playerid, int clubid, int newContractYears);

        /// <summary>
        /// Change date of contract.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newContractDate">New date of contract.</param>
        void ChangeContractDate(int playerid, int clubid, string newContractDate);

        /// <summary>
        /// Change the Salary of the player.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newSalary">New value of Salary.</param>
        void ChangeSalary(int playerid, int clubid, int newSalary);

        /// <summary>
        /// Change the Release clause.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newReleaseClause">New Release clause of the player.</param>
        void ChangeReleaseClause(int playerid, int clubid, int newReleaseClause);

        /// <summary>
        /// Change the player agent.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        /// <param name="newAgent">New player agent.</param>
        void ChangeAgent(int playerid, int clubid, string newAgent);

        /// <summary>
        /// Delete a row.
        /// </summary>
        /// <param name="playerid">Searched PlayerId.</param>
        /// <param name="clubid">Searched ClubId.</param>
        void Delete(int playerid, int clubid);

        /// <summary>
        /// Add a new Contract.
        /// </summary>
        /// <param name="playerid">Id of the player you wants to add.</param>
        /// <param name="clubid">Id of the clubs who signed the player.</param>
        /// <param name="contractyears">Contract years.</param>
        /// <param name="contractdate">Date of the contract.</param>
        /// <param name="salary">Salary in euro.</param>
        /// <param name="releaseclause">Release Clause in euro.</param>
        /// <param name="agent">Player Agent.</param>
        public void AddContract(int playerid, int clubid, int contractyears, string contractdate, int salary, int releaseclause, string agent);

        /// <summary>
        /// Get All Contracts.
        /// </summary>
        /// <returns>Returns all the Contracts.</returns>
        IList<Contract> GetAllContracts();
    }
}
