var namespace_football_management_1_1_data =
[
    [ "Model", "namespace_football_management_1_1_data_1_1_model.html", "namespace_football_management_1_1_data_1_1_model" ],
    [ "Club", "class_football_management_1_1_data_1_1_club.html", "class_football_management_1_1_data_1_1_club" ],
    [ "Contract", "class_football_management_1_1_data_1_1_contract.html", "class_football_management_1_1_data_1_1_contract" ],
    [ "FootballContext", "class_football_management_1_1_data_1_1_football_context.html", "class_football_management_1_1_data_1_1_football_context" ],
    [ "Player", "class_football_management_1_1_data_1_1_player.html", "class_football_management_1_1_data_1_1_player" ]
];