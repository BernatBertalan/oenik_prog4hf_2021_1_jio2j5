var class_football_management_1_1_repository_1_1_player_repository =
[
    [ "PlayerRepository", "class_football_management_1_1_repository_1_1_player_repository.html#ab374f4abaf0f215ef0b7ea1ebfd8abb5", null ],
    [ "AddPlayer", "class_football_management_1_1_repository_1_1_player_repository.html#a2db9639aef9effe1fe04adce32516305", null ],
    [ "ChangeAge", "class_football_management_1_1_repository_1_1_player_repository.html#a2c71efcaa38e0471866a4d59e47bda6c", null ],
    [ "ChangeGoals", "class_football_management_1_1_repository_1_1_player_repository.html#aec739a5b7c2806ca14b7fc0182318986", null ],
    [ "ChangeName", "class_football_management_1_1_repository_1_1_player_repository.html#a295ae649f06cd75038ce90fa6078b625", null ],
    [ "ChangeNation", "class_football_management_1_1_repository_1_1_player_repository.html#a1a74b5541bf7bad31d98535209914415", null ],
    [ "ChangePosition", "class_football_management_1_1_repository_1_1_player_repository.html#a89e74ba3dc9da60d40cc77f7ad7bee34", null ],
    [ "Delete", "class_football_management_1_1_repository_1_1_player_repository.html#a01595c8d137bc2562d2356f00934cc85", null ],
    [ "GetOne", "class_football_management_1_1_repository_1_1_player_repository.html#a927047dfa030fb0379cd26d34690a985", null ]
];