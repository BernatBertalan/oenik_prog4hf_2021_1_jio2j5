var namespace_football_management_1_1_web =
[
    [ "Controllers", "namespace_football_management_1_1_web_1_1_controllers.html", "namespace_football_management_1_1_web_1_1_controllers" ],
    [ "Models", "namespace_football_management_1_1_web_1_1_models.html", "namespace_football_management_1_1_web_1_1_models" ],
    [ "Program", "class_football_management_1_1_web_1_1_program.html", "class_football_management_1_1_web_1_1_program" ],
    [ "Startup", "class_football_management_1_1_web_1_1_startup.html", "class_football_management_1_1_web_1_1_startup" ]
];