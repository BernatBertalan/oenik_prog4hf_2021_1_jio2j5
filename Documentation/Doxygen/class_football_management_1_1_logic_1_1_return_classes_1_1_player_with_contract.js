var class_football_management_1_1_logic_1_1_return_classes_1_1_player_with_contract =
[
    [ "Equals", "class_football_management_1_1_logic_1_1_return_classes_1_1_player_with_contract.html#a64b9926cd5589d3003e9573635cf350b", null ],
    [ "GetHashCode", "class_football_management_1_1_logic_1_1_return_classes_1_1_player_with_contract.html#a914adc78afe2da906c8a9af1f6460884", null ],
    [ "ToString", "class_football_management_1_1_logic_1_1_return_classes_1_1_player_with_contract.html#a100fc9d1c2abae5b817e89764d4be75b", null ],
    [ "Age", "class_football_management_1_1_logic_1_1_return_classes_1_1_player_with_contract.html#a3970a58d47493ee0084417698c9e2a5c", null ],
    [ "ContractDate", "class_football_management_1_1_logic_1_1_return_classes_1_1_player_with_contract.html#a72880bfc123a64a7b4fa72e087de52c9", null ],
    [ "ContractYears", "class_football_management_1_1_logic_1_1_return_classes_1_1_player_with_contract.html#a1369caf70f961d3c49f5e597aa915ddb", null ],
    [ "PlayerName", "class_football_management_1_1_logic_1_1_return_classes_1_1_player_with_contract.html#a9ce1023496746b3fc3b80ca3bf34ed8c", null ]
];