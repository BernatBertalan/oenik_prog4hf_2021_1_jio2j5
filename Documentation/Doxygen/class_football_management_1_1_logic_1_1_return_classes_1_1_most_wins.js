var class_football_management_1_1_logic_1_1_return_classes_1_1_most_wins =
[
    [ "Equals", "class_football_management_1_1_logic_1_1_return_classes_1_1_most_wins.html#ab2e44cc87e602dce94cb5a49f94bac85", null ],
    [ "GetHashCode", "class_football_management_1_1_logic_1_1_return_classes_1_1_most_wins.html#ad953f29176d7254a5c24bd029055fc63", null ],
    [ "ToString", "class_football_management_1_1_logic_1_1_return_classes_1_1_most_wins.html#a2be3fcc64688350e897098943b848f5f", null ],
    [ "ChampionshipWins", "class_football_management_1_1_logic_1_1_return_classes_1_1_most_wins.html#adb5a26e66bc4be2ea1be020287b09563", null ],
    [ "ClubName", "class_football_management_1_1_logic_1_1_return_classes_1_1_most_wins.html#a5b7c10fbc40f41981dfa578685548b2a", null ]
];