﻿// <copyright file="IMainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// IMainLogic interface.
    /// </summary>
    public interface IMainLogic
    {
        /// <summary>
        /// Sending Message.
        /// </summary>
        /// <param name="success">True or False.</param>
        void SendMessage(bool success);

        /// <summary>
        /// Calls PlayersApi controller getall.
        /// </summary>
        /// <returns>List of players.</returns>
        List<PlayerVM> ApiGetPlayers();

        /// <summary>
        /// Calls PlayersApi controller delete.
        /// </summary>
        /// <param name="player">Player we want to edit.</param>
        void ApiDelPlayer(PlayerVM player);

        /// <summary>
        /// Calls PlayersApi controller edit.
        /// </summary>
        /// <param name="player">Player we want to edit.</param>
        /// <param name="isEditing">Is editing now.</param>
        /// <returns>True or false.</returns>
        bool ApiEditPlayer(PlayerVM player, bool isEditing);

        /// <summary>
        /// Implements it is add or edit.
        /// </summary>
        /// <param name="player">Player we want to edit.</param>
        /// <param name="editorFunc">Func that will be implement in MainWindow.</param>
        void EditPlayer(PlayerVM player, Func<PlayerVM, bool> editorFunc);
    }
}
