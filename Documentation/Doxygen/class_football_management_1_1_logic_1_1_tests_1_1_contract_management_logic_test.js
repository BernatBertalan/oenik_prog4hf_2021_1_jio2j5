var class_football_management_1_1_logic_1_1_tests_1_1_contract_management_logic_test =
[
    [ "TestAddNewPlayer", "class_football_management_1_1_logic_1_1_tests_1_1_contract_management_logic_test.html#ab8a94266c8c27d7ab20fb764e24f2007", null ],
    [ "TestDeletePlayer", "class_football_management_1_1_logic_1_1_tests_1_1_contract_management_logic_test.html#aacf43c24e1582307209ae51eb50b9d42", null ],
    [ "TestGetOnePlayer", "class_football_management_1_1_logic_1_1_tests_1_1_contract_management_logic_test.html#a3b929e061aa0449fe4247267e0815ecc", null ],
    [ "TestLessThanTwoYearContract", "class_football_management_1_1_logic_1_1_tests_1_1_contract_management_logic_test.html#a5af420ebd946eadbccdc644c3a6825c9", null ],
    [ "TestTopFiveHighestSalariesOver30", "class_football_management_1_1_logic_1_1_tests_1_1_contract_management_logic_test.html#a4289cb7a55c554ce02497c9aed6ed84b", null ]
];