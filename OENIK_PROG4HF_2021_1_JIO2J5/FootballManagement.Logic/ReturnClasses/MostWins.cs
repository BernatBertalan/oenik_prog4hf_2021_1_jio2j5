﻿// <copyright file="MostWins.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Logic.ReturnClasses
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// MostWins class for NON-CRUDS.
    /// </summary>
    public class MostWins
    {
        /// <summary>
        /// Gets or sets Name of club.
        /// </summary>
        public string ClubName { get; set; }

        /// <summary>
        ///  Gets or sets number of Championship wins.
        /// </summary>
        public int ChampionshipWins { get; set; }

        /// <summary>
        /// Overrided toString, to show noncrud.
        /// </summary>
        /// <returns>Formed string.</returns>
        public override string ToString()
        {
            return $"{this.ClubName} - Championship titles: {this.ChampionshipWins}";
        }

        /// <summary>
        /// Overrided Equals.
        /// </summary>
        /// <param name="obj">An object.</param>
        /// <returns>Returns true or false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is MostWins)
            {
                MostWins other = obj as MostWins;
                return this.ChampionshipWins == other.ChampionshipWins &&
                    this.ClubName == other.ClubName;
            }

            return false;
        }

        /// <summary>
        /// Overrided GetHashCode.
        /// </summary>
        /// <returns>Obect hashcode.</returns>
        public override int GetHashCode()
        {
            return this.ClubName.GetHashCode(System.StringComparison.CurrentCulture) + (int)this.ChampionshipWins;
        }
    }
}
