﻿// <copyright file="IFootballLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Program.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballManagement.Program.Data;

    /// <summary>
    /// Football Logic interface.
    /// </summary>
    public interface IFootballLogic
    {
        /// <summary>
        /// Add a player.
        /// </summary>
        /// <param name="list">The list of the players.</param>
        void AddPlayer(IList<Player> list);

        /// <summary>
        /// Modify a player.
        /// </summary>
        /// <param name="playertoModify">The player we want to modify.</param>
        void ModPlayer(Player playertoModify);

        /// <summary>
        /// Delete a player.
        /// </summary>
        /// <param name="list">The list of the players.</param>
        /// <param name="player">The player we want to delete.</param>
        void DelPlayer(IList<Player> list, Player player);

        /// <summary>
        /// Add a club.
        /// </summary>
        /// <param name="list">The list of the clubs.</param>
        void AddClub(IList<Club> list);

        /// <summary>
        /// Modify a club.
        /// </summary>
        /// <param name="clubtoModify">The club we want to modify.</param>
        void ModClub(Club clubtoModify);

        /// <summary>
        /// Delete a player.
        /// </summary>
        /// <param name="list">The list of the clubs.</param>
        /// <param name="club">The club we want to delete.</param>
        void DelClub(IList<Club> list, Club club);

        /// <summary>
        /// Add a contract.
        /// </summary>
        /// <param name="list">The list of the contracts.</param>
        void AddContract(IList<Contract> list);

        /// <summary>
        /// Modify a contract.
        /// </summary>
        /// <param name="contracttoModify">The contract we want to modify.</param>
        void ModContract(Contract contracttoModify);

        /// <summary>
        /// Delete a player.
        /// </summary>
        /// <param name="list">The list of the contracts.</param>
        /// <param name="contract">The contract we want to delete.</param>
        void DelContract(IList<Contract> list, Contract contract);

        /// <summary>
        /// Get all players method.
        /// </summary>
        /// <returns>List of players.</returns>
        IList<Player> GetAllPlayers();

        /// <summary>
        /// Get all players method.
        /// </summary>
        /// <returns>List of players.</returns>
        IList<Club> GetAllClub();

        /// <summary>
        /// Get all players method.
        /// </summary>
        /// <returns>List of players.</returns>
        IList<Contract> GetAllContract();
    }
}
