var class_football_management_1_1_logic_1_1_club_leader_logic =
[
    [ "ClubLeaderLogic", "class_football_management_1_1_logic_1_1_club_leader_logic.html#afee053d47e8534c6177957f505745dbe", null ],
    [ "AddClub", "class_football_management_1_1_logic_1_1_club_leader_logic.html#a1b356a399551d7ed33ad84ee1d0cdeec", null ],
    [ "ChangeChampionshipWins", "class_football_management_1_1_logic_1_1_club_leader_logic.html#a13b595d4a5a194d1f3d2fa8d1a1ac85d", null ],
    [ "ChangeCoach", "class_football_management_1_1_logic_1_1_club_leader_logic.html#af078e15a6fd7e4eb8ec58f9101a28616", null ],
    [ "ChangeCountry", "class_football_management_1_1_logic_1_1_club_leader_logic.html#a39911e65101d56e68dbe45bb5d1c4f58", null ],
    [ "ChangeFounded", "class_football_management_1_1_logic_1_1_club_leader_logic.html#aeab3d2592f044640b33c56fc87e56004", null ],
    [ "ChangeName", "class_football_management_1_1_logic_1_1_club_leader_logic.html#a07506ef340cb10ecace6e60a8b9bbebf", null ],
    [ "Delete", "class_football_management_1_1_logic_1_1_club_leader_logic.html#a264eed76c0f572be7c595e664744f886", null ],
    [ "GetAllClubs", "class_football_management_1_1_logic_1_1_club_leader_logic.html#a585f96c494b7037ddb18eb9494b11259", null ],
    [ "GetClubById", "class_football_management_1_1_logic_1_1_club_leader_logic.html#a8c046056e1b5d5c1585bbba6aba1b535", null ],
    [ "GoalsPerClubs", "class_football_management_1_1_logic_1_1_club_leader_logic.html#ab0139693dc5854847e3240dafd027193", null ],
    [ "GoalsPerClubsAsync", "class_football_management_1_1_logic_1_1_club_leader_logic.html#a58f0a48ebcb74d24fae40df47081e6b4", null ],
    [ "MostChampionshipWins", "class_football_management_1_1_logic_1_1_club_leader_logic.html#ac2e3caafbeed4947ce1a15400f06093c", null ],
    [ "MostChampionshipWinsAsync", "class_football_management_1_1_logic_1_1_club_leader_logic.html#ae0992ae726c3ee64ee277b0bda78825e", null ]
];