var class_football_management_1_1_program_1_1_b_l_1_1_football_logic =
[
    [ "FootballLogic", "class_football_management_1_1_program_1_1_b_l_1_1_football_logic.html#a7127f7ab347ddceb67d042a5fba2cf09", null ],
    [ "AddClub", "class_football_management_1_1_program_1_1_b_l_1_1_football_logic.html#ab85030b63aec9694fd5ad01a8a99c086", null ],
    [ "AddContract", "class_football_management_1_1_program_1_1_b_l_1_1_football_logic.html#a075b5f2dce88718eb603f742052e8089", null ],
    [ "AddPlayer", "class_football_management_1_1_program_1_1_b_l_1_1_football_logic.html#a726f2c433e2ae5ff68e05d1fe427dff9", null ],
    [ "DelClub", "class_football_management_1_1_program_1_1_b_l_1_1_football_logic.html#a94bc7371ffea17dda26bdc1964f55415", null ],
    [ "DelContract", "class_football_management_1_1_program_1_1_b_l_1_1_football_logic.html#a99fa43c7620eca48caaec962738a263d", null ],
    [ "DelPlayer", "class_football_management_1_1_program_1_1_b_l_1_1_football_logic.html#a412366019733cefe68be4dca12665498", null ],
    [ "GetAllClub", "class_football_management_1_1_program_1_1_b_l_1_1_football_logic.html#ab772fe9e4c68cf7e2870eafbfc0cab9d", null ],
    [ "GetAllContract", "class_football_management_1_1_program_1_1_b_l_1_1_football_logic.html#a0e8dc7023cbe474bf341d1aade5fa74e", null ],
    [ "GetAllPlayers", "class_football_management_1_1_program_1_1_b_l_1_1_football_logic.html#ab8bf66101383ec00e650e9ea7277ea23", null ],
    [ "ModClub", "class_football_management_1_1_program_1_1_b_l_1_1_football_logic.html#ae40ddd1d86a9b411eb17d747c1f82089", null ],
    [ "ModContract", "class_football_management_1_1_program_1_1_b_l_1_1_football_logic.html#ad16aeba153b7c07f6d6763dede5b97d5", null ],
    [ "ModPlayer", "class_football_management_1_1_program_1_1_b_l_1_1_football_logic.html#aaf9d404c63961ef221b0fbd36c4beb5a", null ]
];