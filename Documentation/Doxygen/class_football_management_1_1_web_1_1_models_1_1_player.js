var class_football_management_1_1_web_1_1_models_1_1_player =
[
    [ "Age", "class_football_management_1_1_web_1_1_models_1_1_player.html#a1d430946bbb09e700dff099bc06c1484", null ],
    [ "Goals", "class_football_management_1_1_web_1_1_models_1_1_player.html#a0f893ef95c9b7052cda4a36e2fc967d8", null ],
    [ "Name", "class_football_management_1_1_web_1_1_models_1_1_player.html#a317813f0b3538926acc6b81b1a63a43c", null ],
    [ "Nation", "class_football_management_1_1_web_1_1_models_1_1_player.html#af7c4b1c7439d4cfee6cdd39e99b882de", null ],
    [ "PlayerId", "class_football_management_1_1_web_1_1_models_1_1_player.html#a977ae7914e05b634b24891ad31af00df", null ],
    [ "Position", "class_football_management_1_1_web_1_1_models_1_1_player.html#aa3dfe098fe6a092b6c9eac5d3a8fe3f9", null ]
];