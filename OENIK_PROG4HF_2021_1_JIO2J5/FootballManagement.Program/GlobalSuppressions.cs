﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "Cant use using.", Scope = "member", Target = "~M:FootballManagement.Program.BL.FootballLogic.#ctor(FootballManagement.Program.BL.IEditorService,GalaSoft.MvvmLight.Messaging.IMessenger)")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "Cant resolve.", Scope = "type", Target = "~T:FootballManagement.Program.App")]
