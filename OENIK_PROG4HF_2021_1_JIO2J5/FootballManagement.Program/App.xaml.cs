﻿// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Program
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using CommonServiceLocator;
    using FootballManagement.Data;
    using FootballManagement.Logic;
    using FootballManagement.Program.BL;
    using FootballManagement.Program.UI;
    using FootballManagement.Repository;
    using FootballManagement.Repository.Interfaces;
    using GalaSoft.MvvmLight.Ioc;
    using GalaSoft.MvvmLight.Messaging;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => MyIoc.Instance);

            MyIoc.Instance.Register<DbContext>(() => new FootballContext());
            MyIoc.Instance.Register<IEditorService, EditorServiceViaWindow>();
            MyIoc.Instance.Register<IMessenger>(() => Messenger.Default);

            MyIoc.Instance.Register<IPlayerRepository, PlayerRepository>();
            MyIoc.Instance.Register<IContractRepository, ContractRepository>();
            MyIoc.Instance.Register<IClubRepository, ClubRepository>();

            MyIoc.Instance.Register<IClubLeaderLogic, ClubLeaderLogic>();
            MyIoc.Instance.Register<IContractManagementLogic, ContractManagementLogic>();

            MyIoc.Instance.Register<IFootballLogic, FootballLogic>();
        }
    }
}
