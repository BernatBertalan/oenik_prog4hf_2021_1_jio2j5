﻿// <copyright file="ClubEditorWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Program.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using FootballManagement.Program.Data;
    using FootballManagement.Program.VM;

    /// <summary>
    /// Interaction logic for ClubEditorWindow.xaml.
    /// </summary>
    public partial class ClubEditorWindow : Window
    {
        private ClubEditorViewModel vM;

        /// <summary>
        /// Initializes a new instance of the <see cref="ClubEditorWindow"/> class.
        /// </summary>
        public ClubEditorWindow()
        {
            this.InitializeComponent();

            this.vM = this.FindResource("VM") as ClubEditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClubEditorWindow"/> class.
        /// </summary>
        /// <param name="oldclub">An input club.</param>
        public ClubEditorWindow(Club oldclub)
            : this()
        {
            this.vM.Club = oldclub;
        }

        private void OkClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
