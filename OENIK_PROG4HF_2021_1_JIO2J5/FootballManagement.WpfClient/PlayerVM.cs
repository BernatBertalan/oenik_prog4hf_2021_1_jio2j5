﻿// <copyright file="PlayerVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// PlayerVM class implements ObservableObject.
    /// </summary>
    public class PlayerVM : ObservableObject
    {
        private int playerid;
        private string name;
        private int age;
        private int goals;
        private string nation;
        private string position;

        /// <summary>
        /// Gets or Sets ID of the player.
        /// </summary>
        public int PlayerId
        {
            get { return this.playerid; }
            set { this.Set(ref this.playerid, value); }
        }

        /// <summary>
        /// Gets or Sets Name of the player.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or Sets Age of the player.
        /// </summary>
        public int Age
        {
            get { return this.age; }
            set { this.Set(ref this.age, value); }
        }

        /// <summary>
        /// Gets or Sets number of Goals.
        /// </summary>
        public int Goals
        {
            get { return this.goals; }
            set { this.Set(ref this.goals, value); }
        }

        /// <summary>
        /// Gets or Sets Nation of the player.
        /// </summary>
        public string Nation
        {
            get { return this.nation; }
            set { this.Set(ref this.nation, value); }
        }

        /// <summary>
        /// Gets or Sets Position on the pitch.
        /// </summary>
        public string Position
        {
            get { return this.position; }
            set { this.Set(ref this.position, value); }
        }

        /// <summary>
        /// CopyFrom method.
        /// </summary>
        /// <param name="other"> Other Player that we want to copy.</param>
        public void CopyFrom(PlayerVM other)
        {
            if (other == null)
            {
                return;
            }

            this.Name = other.Name;
            this.Position = other.Position;
            this.Nation = other.Nation;
            this.Age = other.Age;
            this.Goals = other.Goals;
            this.PlayerId = other.PlayerId;
        }
    }
}
