var class_football_management_1_1_program_1_1_data_1_1_player =
[
    [ "CopyFrom", "class_football_management_1_1_program_1_1_data_1_1_player.html#a59c46a19227cb0ede3d23dc5134547df", null ],
    [ "Age", "class_football_management_1_1_program_1_1_data_1_1_player.html#abb207787f91b0d0833c921c51591a27f", null ],
    [ "Goals", "class_football_management_1_1_program_1_1_data_1_1_player.html#a551eb653e4ffcc8b8205c2ad213685b4", null ],
    [ "Name", "class_football_management_1_1_program_1_1_data_1_1_player.html#a00e13d7c6d57e7698d75b009e8c9c38d", null ],
    [ "Nation", "class_football_management_1_1_program_1_1_data_1_1_player.html#acd49b18381a74934994a062e40dcc791", null ],
    [ "PlayerId", "class_football_management_1_1_program_1_1_data_1_1_player.html#a3645f86a91ad84f071f1c3de9346afed", null ],
    [ "Position", "class_football_management_1_1_program_1_1_data_1_1_player.html#af111c616e708111a748249e7a1bf862d", null ]
];