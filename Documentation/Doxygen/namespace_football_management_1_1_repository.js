var namespace_football_management_1_1_repository =
[
    [ "Interfaces", "namespace_football_management_1_1_repository_1_1_interfaces.html", "namespace_football_management_1_1_repository_1_1_interfaces" ],
    [ "ClubRepository", "class_football_management_1_1_repository_1_1_club_repository.html", "class_football_management_1_1_repository_1_1_club_repository" ],
    [ "ContractRepository", "class_football_management_1_1_repository_1_1_contract_repository.html", "class_football_management_1_1_repository_1_1_contract_repository" ],
    [ "FootballRepository", "class_football_management_1_1_repository_1_1_football_repository.html", "class_football_management_1_1_repository_1_1_football_repository" ],
    [ "PlayerRepository", "class_football_management_1_1_repository_1_1_player_repository.html", "class_football_management_1_1_repository_1_1_player_repository" ],
    [ "IFootballRepository", "interface_football_management_1_1_repository_1_1_i_football_repository.html", "interface_football_management_1_1_repository_1_1_i_football_repository" ]
];