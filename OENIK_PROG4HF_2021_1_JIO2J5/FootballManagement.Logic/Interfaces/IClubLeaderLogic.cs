﻿// <copyright file="IClubLeaderLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using FootballManagement.Data;

    /// <summary>
    /// IClubLogic interface.
    /// </summary>
    public interface IClubLeaderLogic
    {
        /// <summary>
        /// Get Club By Id.
        /// </summary>
        /// <param name="id">Searched ClubId.</param>
        /// <returns>The club with given id.</returns>
        Club GetClubById(int id);

        /// <summary>
        /// Change name of the club.
        /// </summary>
        /// <param name="id">Searched clubId.</param>
        /// <param name="newName">New name of the club.</param>
        void ChangeName(int id, string newName);

        /// <summary>
        /// Change Country of the club.
        /// </summary>
        /// <param name="id">Searched clubId.</param>
        /// <param name="newCountry">New Country of the club.</param>
        void ChangeCountry(int id, string newCountry);

        /// <summary>
        /// Change Coach of the club.
        /// </summary>
        /// <param name="id">Searched clubId.</param>
        /// <param name="newCoach">New Coach of the club.</param>
        void ChangeCoach(int id, string newCoach);

        /// <summary>
        /// Change when was the club Founded.
        /// </summary>
        /// <param name="id">Searched clubId.</param>
        /// <param name="newFounded">New Founded year.</param>
        void ChangeFounded(int id, int newFounded);

        /// <summary>
        /// Change number of Championship wins.
        /// </summary>
        /// <param name="id">Searched ClubId.</param>
        /// <param name="newWins">New number of Wins.</param>
        void ChangeChampionshipWins(int id, int newWins);

        /// <summary>
        /// Delete a row.
        /// </summary>
        /// <param name="id">ClubId to delete.</param>
        void Delete(int id);

        /// <summary>
        /// Add a new Club.
        /// </summary>
        /// <param name="name">Nameof the Club.</param>
        /// <param name="coach">Name of the coach.</param>
        /// <param name="founded">When was it founded.</param>
        /// <param name="country">Country of the club.</param>
        /// <param name="championshipWins">Number of Championship wins.</param>
        public void AddClub(string name, string coach, int founded, string country, int championshipWins);

        /// <summary>
        /// Get All Clubs.
        /// </summary>
        /// <returns>Returns all the clubs.</returns>
        IList<Club> GetAllClubs();
    }
}
