﻿// <copyright file="PlayersController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using FootballManagement.Logic;
    using FootballManagement.Web.Models;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Controller of the application.
    /// </summary>
    public class PlayersController : Controller
    {
        private IContractManagementLogic logic;
        private IMapper mapper;
        private PlayerListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="PlayersController"/> class.
        /// </summary>
        /// <param name="logic"> ContractManagement Logic.</param>
        /// <param name="mapper">Mapper interface.</param>
        public PlayersController(IContractManagementLogic logic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;

            this.vm = new PlayerListViewModel();
            this.vm.EditedPlayer = new Models.Player();

            var players = logic.GetAllPlayers();
            this.vm.ListOfPlayers = mapper.Map<IList<FootballManagement.Data.Player>, List<Models.Player>>(players);
        }

        /// <summary>
        /// Remove player.
        /// </summary>
        /// <param name="id">Id of the player.</param>
        /// <returns>Redirect to Index page.</returns>
        public IActionResult Remove(int id)
        {
            this.TempData["editResult"] = "Delete FAIL";
            if (this.logic.DeletePlayer(id))
            {
                this.TempData["editResult"] = "Delete OK";
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Index page action.
        /// </summary>
        /// <returns>A view of Index page.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("PlayersIndex", this.vm);
        }

        /// <summary>
        /// Shows player edit on index page.
        /// </summary>
        /// <param name="id">Id of the player.</param>
        /// <returns>A View on index page.</returns>
        public IActionResult Edit(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.vm.EditedPlayer = this.GetPlayerModel(id);
            return this.View("PlayersIndex", this.vm);
        }

        /// <summary>
        /// Edit the selected Player.
        /// </summary>
        /// <param name="player">Selected player.</param>
        /// <param name="editAction">If Add new add to database, if not just change details.</param>
        /// <returns>A view of index page.</returns>
        [HttpPost]
        public IActionResult Edit(Models.Player player, string editAction)
        {
            if (this.ModelState.IsValid && player != null)
            {
                this.TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    this.logic.AddPlayer(player.Name, player.Age, player.Goals, player.Nation, player.Position);
                }
                else
                {
                    if (!this.logic.ChangePlayer(player.PlayerId, player.Name, player.Age, player.Nation, player.Goals, player.Position))
                    {
                        this.TempData["editResult"] = "Edit FAIL";
                    }
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.vm.EditedPlayer = player;
                return this.View("PlayerIndex", this.vm);
            }
        }

        /// <summary>
        /// Shows the details of the player.
        /// </summary>
        /// <param name="id">Id of the player.</param>
        /// <returns>A View of PlayerDetails.</returns>
        public IActionResult Details(int id)
        {
            return this.View("PlayersDetails", this.GetPlayerModel(id));
        }

        private Models.Player GetPlayerModel(int id)
        {
            FootballManagement.Data.Player onePlayer = this.logic.GetPlayerById(id);
            return this.mapper.Map<FootballManagement.Data.Player, Models.Player>(onePlayer);
        }
    }
}
