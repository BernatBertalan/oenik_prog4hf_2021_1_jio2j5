﻿// <copyright file="FootballRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Repository abstract class.
    /// </summary>
    /// <typeparam name="T">Type of the current Repository.</typeparam>
    public abstract class FootballRepository<T> : IFootballRepository<T>
        where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FootballRepository{T}"/> class.
        /// </summary>
        /// <param name="ctx">DbContext context.</param>
        public FootballRepository(DbContext ctx)
        {
            this.Ctx = ctx;
        }

        /// <summary>
        /// Gets or sets dbContext Context.
        /// </summary>
        public DbContext Ctx { get; set; }

        /// <summary>
        /// Get All element of the tables.
        /// </summary>
        /// <returns>All elements of the table.</returns>
        public IQueryable<T> GetAll()
        {
            return this.Ctx.Set<T>();
        }
    }
}
