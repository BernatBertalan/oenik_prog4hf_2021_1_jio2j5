﻿// <copyright file="ContractEditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Program.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FootballManagement.Program.Data;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Implements ViewModelBase class.
    /// </summary>
    public class ContractEditorViewModel : ViewModelBase
    {
        private Contract contract;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContractEditorViewModel"/> class.
        /// </summary>
        public ContractEditorViewModel()
        {
            this.contract = new Contract();
            if (this.IsInDesignMode)
            {
                this.contract.Agent = "Raviola";
                this.contract.ContractDate = "2021.03.08";
                this.contract.ContractYears = 2;
            }
        }

        /// <summary>
        /// Gets or sets a Contract to edit.
        /// </summary>
        public Contract Contract
        {
            get { return this.contract; }
            set { this.Set(ref this.contract, value); }
        }
    }
}
