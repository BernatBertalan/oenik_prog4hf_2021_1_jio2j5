var class_football_management_1_1_program_1_1_data_1_1_contract =
[
    [ "CopyFrom", "class_football_management_1_1_program_1_1_data_1_1_contract.html#a12184499eeb5dc6cda318afbed0158ea", null ],
    [ "Agent", "class_football_management_1_1_program_1_1_data_1_1_contract.html#a52d10ea9dd3524f4d09ff491a3ae25b5", null ],
    [ "ClubId", "class_football_management_1_1_program_1_1_data_1_1_contract.html#ab9b8baebc5a5daf70caf7ef25c8a4bf8", null ],
    [ "ContractDate", "class_football_management_1_1_program_1_1_data_1_1_contract.html#a3052215a0ed0875dc8b7eca38557570b", null ],
    [ "ContractYears", "class_football_management_1_1_program_1_1_data_1_1_contract.html#a80ab2e5f4e0b53c1782ff3e22db90596", null ],
    [ "PlayerId", "class_football_management_1_1_program_1_1_data_1_1_contract.html#a457f8c3bcd39fa378e7a0078cb44b607", null ],
    [ "ReleaseClause", "class_football_management_1_1_program_1_1_data_1_1_contract.html#ab2d8a8bd3334544bf171acae314fc0bb", null ],
    [ "Salary", "class_football_management_1_1_program_1_1_data_1_1_contract.html#a9659b9a1c54244c0e080e74a1509d349", null ]
];