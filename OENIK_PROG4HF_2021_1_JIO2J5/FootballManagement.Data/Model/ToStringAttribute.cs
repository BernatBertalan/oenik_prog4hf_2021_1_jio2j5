﻿// <copyright file="ToStringAttribute.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Data.Model
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// ToString Attribute to help writing out.
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    public class ToStringAttribute : Attribute
    {
    }
}
