﻿// <copyright file="Contract.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Data
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Reflection;
    using FootballManagement.Data.Model;

    /// <summary>
    /// Clubs Table.
    /// </summary>
    [Table("Contracts")]
    public class Contract
    {
        /// <summary>
        /// Gets or Sets ClubId, references clubs table.
        /// </summary>
        [Required]
        [ForeignKey(nameof(Club))]
        [ToString]
        public int ClubId { get; set; }

        /// <summary>
        /// Gets or Sets Club.
        /// </summary>
        [NotMapped]
        public virtual Club Club { get; set; }

        /// <summary>
        /// Gets or Sets PlayerId, references players table.
        /// </summary>
        [Required]
        [ForeignKey(nameof(Player))]
        [ToString]
        public int PlayerId { get; set; }

        /// <summary>
        /// Gets or Sets Player.
        /// </summary>
        [NotMapped]
        public virtual Player Player { get; set; }

        /// <summary>
        /// Gets or Sets date of contract.
        /// </summary>
        [Required]
        [ToString]
        public string ContractDate { get; set; }

        /// <summary>
        /// Gets or Sets number of Contract years.
        /// </summary>
        [Required]
        [MaxLength(2)]
        [ToString]
        public int ContractYears { get; set; }

        /// <summary>
        /// Gets or Sets salary in Euro.
        /// </summary>
        [Required]
        [ToString]
        public int Salary { get; set; }

        /// <summary>
        /// Gets or Sets releaseClause in Euro.
        /// </summary>
        [Required]
        [ToString]
        public int ReleaseClause { get; set; }

        /// <summary>
        /// Gets or Sets the player agent.
        /// </summary>
        [Required]
        [ToString]
        public string Agent { get; set; }

        /// <summary>
        /// Gets MainData to write out.
        /// </summary>
        public string MainData => $"\n Date: {this.ContractDate} \n Contract Years: {this.ContractYears} \n Salary: {this.Salary} \n Release clause: {this.ReleaseClause} \n Manager: {this.Agent}";

        /// <summary>
        /// Overrided to String.
        /// </summary>
        /// <returns>Only the selected elements in the table.</returns>
        public override string ToString()
        {
            string x = string.Empty;

            foreach (var item in this.GetType().GetProperties().Where(x => x.GetCustomAttribute<ToStringAttribute>() != null))
            {
                x += "  ";
                x += item.Name + ":  ";
                x += item.GetValue(this);
                x += "\n";
            }

            return x;
        }

        /// <summary>
        /// Overrided equals method.
        /// </summary>
        /// <param name="obj">An object. </param>
        /// <returns>Return true or false.</returns>
        public override bool Equals(object obj)
        {
            if (obj is Contract)
            {
                Contract other = obj as Contract;
                return this.PlayerId == other.PlayerId && this.ClubId == other.ClubId && this.PlayerId == other.PlayerId && this.ContractYears == other.ContractYears && this.ContractDate == other.ContractDate && this.Agent == other.Agent && this.Salary == other.Salary && this.ReleaseClause == other.ReleaseClause;
            }

            return false;
        }

        /// <summary>
        /// Overrided GetHashCode.
        /// </summary>
        /// <returns>Obect hashcode.</returns>
        public override int GetHashCode()
        {
            return (int)this.ClubId + (int)this.PlayerId;
        }
    }
}
