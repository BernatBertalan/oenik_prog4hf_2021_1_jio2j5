﻿// <copyright file="App.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => MyIoc.Instance);
            MyIoc.Instance.Register<IMainLogic, MainLogic>();
        }

        /// <summary>
        /// MyIoc implements SimpleIoc and IServiceLocator interface.
        /// </summary>
        public class MyIoc : SimpleIoc, IServiceLocator
        {
            /// <summary>
            /// Gets Instance to help implement App.xaml.cs.
            /// </summary>
            public static MyIoc Instance { get; private set; } = new MyIoc();
        }
    }
}
