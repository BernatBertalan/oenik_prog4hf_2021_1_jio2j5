var class_football_management_1_1_data_1_1_player =
[
    [ "Player", "class_football_management_1_1_data_1_1_player.html#a61d29fdf37c30f58ca7bec4f91aaee85", null ],
    [ "Equals", "class_football_management_1_1_data_1_1_player.html#a1a7dd3e4c0bf28416ed104d66cac65e5", null ],
    [ "GetHashCode", "class_football_management_1_1_data_1_1_player.html#a0a3f48f68afc0ef9000a096bfb3ee183", null ],
    [ "ToString", "class_football_management_1_1_data_1_1_player.html#a70513db3844d34f5bb0cb681b5851135", null ],
    [ "Age", "class_football_management_1_1_data_1_1_player.html#a18e894e2a170a805fc4cd049f5f9d050", null ],
    [ "Contracts", "class_football_management_1_1_data_1_1_player.html#ae1e2e990f3986240f473f9ab61ac2bd2", null ],
    [ "Goals", "class_football_management_1_1_data_1_1_player.html#a04e489e2b6a4231020c4178ce0bc255a", null ],
    [ "MainData", "class_football_management_1_1_data_1_1_player.html#a5263ebf076aaf5d98608d5a440448eb1", null ],
    [ "Name", "class_football_management_1_1_data_1_1_player.html#aa714e3b4a8694ebc17da9d9a5571d5d0", null ],
    [ "Nation", "class_football_management_1_1_data_1_1_player.html#a7ec9706ffba1ca2f59fb2196cb652e01", null ],
    [ "PlayerId", "class_football_management_1_1_data_1_1_player.html#ad33163f92d7539bbdf143db83251a65c", null ],
    [ "Position", "class_football_management_1_1_data_1_1_player.html#aa0b14adbc4b4427b09a34bca7f196fcf", null ]
];