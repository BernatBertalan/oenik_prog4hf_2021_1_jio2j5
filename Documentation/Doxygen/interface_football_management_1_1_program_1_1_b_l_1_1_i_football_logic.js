var interface_football_management_1_1_program_1_1_b_l_1_1_i_football_logic =
[
    [ "AddClub", "interface_football_management_1_1_program_1_1_b_l_1_1_i_football_logic.html#a439e44b4933203f5a5eae679bac68ba9", null ],
    [ "AddContract", "interface_football_management_1_1_program_1_1_b_l_1_1_i_football_logic.html#a59ac2b70f41b638b8f484aeb336ab4d7", null ],
    [ "AddPlayer", "interface_football_management_1_1_program_1_1_b_l_1_1_i_football_logic.html#af87ee2243fd4c1cc30fb76d9606bfc3d", null ],
    [ "DelClub", "interface_football_management_1_1_program_1_1_b_l_1_1_i_football_logic.html#adfa28eaf1e82191e9d70391c8f50de86", null ],
    [ "DelContract", "interface_football_management_1_1_program_1_1_b_l_1_1_i_football_logic.html#a266d0a39df30654790595abd3b3dd2c9", null ],
    [ "DelPlayer", "interface_football_management_1_1_program_1_1_b_l_1_1_i_football_logic.html#a1faf2e3332f7a5419540a6aabf43fd74", null ],
    [ "GetAllClub", "interface_football_management_1_1_program_1_1_b_l_1_1_i_football_logic.html#a22dadb845d22e764b0294ad54adc7653", null ],
    [ "GetAllContract", "interface_football_management_1_1_program_1_1_b_l_1_1_i_football_logic.html#ac9531e6ceed7591374eb677414e95ce1", null ],
    [ "GetAllPlayers", "interface_football_management_1_1_program_1_1_b_l_1_1_i_football_logic.html#a9505aea7c67797a7ae0541e72935c420", null ],
    [ "ModClub", "interface_football_management_1_1_program_1_1_b_l_1_1_i_football_logic.html#adce069fe3cc23b12d4a35ac3de7db0e3", null ],
    [ "ModContract", "interface_football_management_1_1_program_1_1_b_l_1_1_i_football_logic.html#ae67267779008c296ab9e234aae1cd2d4", null ],
    [ "ModPlayer", "interface_football_management_1_1_program_1_1_b_l_1_1_i_football_logic.html#a8e4a5cec4fc06623ad13bf557c0be448", null ]
];