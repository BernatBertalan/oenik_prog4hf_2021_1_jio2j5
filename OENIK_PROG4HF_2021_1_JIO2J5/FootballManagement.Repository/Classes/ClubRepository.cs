﻿// <copyright file="ClubRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using FootballManagement.Data;
    using FootballManagement.Repository.Interfaces;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// ClubRepository.
    /// </summary>
    public class ClubRepository : FootballRepository<Club>, IClubRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClubRepository"/> class.
        /// </summary>
        /// <param name="ctx">DbContext context.</param>
        public ClubRepository(DbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Change number of Championship wins.
        /// </summary>
        /// <param name="id">Searched ClubId.</param>
        /// <param name="newWins">New number of Wins.</param>
        public void ChangeChampionshipWins(int id, int newWins)
        {
            if (this.GetOne(id) != null)
            {
                var club = this.GetOne(id);
                club.ChampionshipWins = newWins;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This ID is not exist!");
            }
        }

        /// <summary>
        /// Change Coach of the club.
        /// </summary>
        /// <param name="id">Searched clubId.</param>
        /// <param name="newCoach">New Coach of the club.</param>
        public void ChangeCoach(int id, string newCoach)
        {
            if (this.GetOne(id) != null)
            {
                var club = this.GetOne(id);
                club.Coach = newCoach;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This ID is not exist!");
            }
        }

        /// <summary>
        /// Change Country of the club.
        /// </summary>
        /// <param name="id">Searched clubId.</param>
        /// <param name="newCountry">New Country of the club.</param>
        public void ChangeCountry(int id, string newCountry)
        {
            if (this.GetOne(id) != null)
            {
                var club = this.GetOne(id);
                club.Country = newCountry;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This ID is not exist!");
            }
        }

        /// <summary>
        /// Change when was the club Founded.
        /// </summary>
        /// <param name="id">Searched clubId.</param>
        /// <param name="newFounded">New Founded year.</param>
        public void ChangeFounded(int id, int newFounded)
        {
            if (this.GetOne(id) != null)
            {
                var club = this.GetOne(id);
                club.Founded = newFounded;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This ID is not exist!");
            }
        }

        /// <summary>
        /// Change name of the club.
        /// </summary>
        /// <param name="id">Searched clubId.</param>
        /// <param name="newName">New name of the club.</param>
        public void ChangeName(int id, string newName)
        {
            if (this.GetOne(id) != null)
            {
                var club = this.GetOne(id);
                club.Name = newName;
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This ID is not exist!");
            }
        }

        /// <summary>
        /// Delete a row.
        /// </summary>
        /// <param name="id">ClubId to delete.</param>
        public void Delete(int id)
        {
            if (this.GetOne(id) != null)
            {
                var club = this.GetOne(id);
                this.Ctx.Remove(club);
                this.Ctx.SaveChanges();
            }
            else
            {
                throw new NullReferenceException("This ID is not exist!");
            }
        }

        /// <summary>
        /// Get Club By Id.
        /// </summary>
        /// <param name="id">Searched ClubId.</param>
        /// <returns>The club with given id.</returns>
        public Club GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.ClubId == id);
        }

        /// <summary>
        /// Add a new Club.
        /// </summary>
        /// <param name="name">Nameof the Club.</param>
        /// <param name="coach">Name of the coach.</param>
        /// <param name="founded">When was it founded.</param>
        /// <param name="country">Country of the club.</param>
        /// <param name="championshipWins">Number of Championship wins.</param>
        public void AddClub(string name, string coach, int founded, string country, int championshipWins)
        {
            this.Ctx.Add(new Club() { Name = name, Coach = coach, Founded = founded, Country = country, ChampionshipWins = championshipWins });
            this.Ctx.SaveChanges();
        }
    }
}
