﻿// <copyright file="Club.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Program.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Club models.
    /// </summary>
    public class Club : ObservableObject
    {
        private int clubid;
        private string name;
        private string country;
        private string coach;
        private int founded;
        private int championshipwins;

        /// <summary>
        /// Gets or Sets ID of the club.
        /// </summary>
        public int ClubId
        {
            get { return this.clubid; }
            set { this.Set(ref this.clubid, value); }
        }

        /// <summary>
        /// Gets or Sets Name of the club.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or Sets Country of the club.
        /// </summary>
        public string Country
        {
            get { return this.country; }
            set { this.Set(ref this.country, value); }
        }

        /// <summary>
        /// Gets or Sets Coach of the club.
        /// </summary>
        public string Coach
        {
            get { return this.coach; }
            set { this.Set(ref this.coach, value); }
        }

        /// <summary>
        /// Gets or Sets when was founded.
        /// </summary>
        public int Founded
        {
            get { return this.founded; }
            set { this.Set(ref this.founded, value); }
        }

        /// <summary>
        /// Gets or Sets number of Championship wins.
        /// </summary>
        public int ChampionshipWins
        {
            get { return this.championshipwins; }
            set { this.Set(ref this.championshipwins, value); }
        }

        /// <summary>
        /// CopyFrom method.
        /// </summary>
        /// <param name="other"> Other Club that we want to copy.</param>
        public void CopyFrom(Club other)
        {
            if (other != null)
            {
                this.Name = other.Name;
                this.Founded = other.Founded;
                this.ChampionshipWins = other.ChampionshipWins;
                this.Coach = other.Coach;
                this.Country = other.Country;
                this.ClubId = other.ClubId;
            }
        }
    }
}
