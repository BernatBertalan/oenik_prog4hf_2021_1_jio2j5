var namespace_football_management_1_1_logic =
[
    [ "ReturnClasses", "namespace_football_management_1_1_logic_1_1_return_classes.html", "namespace_football_management_1_1_logic_1_1_return_classes" ],
    [ "Tests", "namespace_football_management_1_1_logic_1_1_tests.html", "namespace_football_management_1_1_logic_1_1_tests" ],
    [ "ClubLeaderLogic", "class_football_management_1_1_logic_1_1_club_leader_logic.html", "class_football_management_1_1_logic_1_1_club_leader_logic" ],
    [ "ContractManagementLogic", "class_football_management_1_1_logic_1_1_contract_management_logic.html", "class_football_management_1_1_logic_1_1_contract_management_logic" ],
    [ "IClubLeaderLogic", "interface_football_management_1_1_logic_1_1_i_club_leader_logic.html", "interface_football_management_1_1_logic_1_1_i_club_leader_logic" ],
    [ "IContractManagementLogic", "interface_football_management_1_1_logic_1_1_i_contract_management_logic.html", "interface_football_management_1_1_logic_1_1_i_contract_management_logic" ]
];