FOOTBALL MANAGEMENT



Function list:

Main menu:

- PLAYERS

- CLUBS

- CONTRACTS

Players menu:

- LIST ALL

- GET BY ID

- CHANGE DETAILS (Another menu to change Name, Age, Goals, Nation, Position)

- ADD PLAYER

- DELETE PLAYER

- TOP 5 HIGHEST PAID PLAYER OVER 30

- ASYNC TOP 5 HIGHEST PAID PLAYER OVER 30


Clubs menu:

- LIST ALL

- GET BY ID

- CHANGE DETAILS (Another menu to change Name, Founded, Coach, Country, Championship Wins)

- ADD CLUB

- DELETE CLUB

- GOALS/CLUBS

- ASYNC GOALS/CLUBS

- MOST CHAMPIONSHIP WINS

- ASYNC  MOST CHAMPIONSHIP WINS


Contracts menu:

- LIST ALL

- GET BY ID

- CHANGE DETAILS (Another menu to change Contract date, Contract years, Agent, Release Clause, Salary)

- ADD CONTRACT

- DELETE CONTRACT

- LIST PLAYERS LESS THAN TWO YEARS CONTRACT

- ASYNC LIST PLAYERS LESS THAN TWO YEARS CONTRACT


