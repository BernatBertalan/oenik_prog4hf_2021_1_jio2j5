﻿// <copyright file="FootballContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace FootballManagement.Data
{
    using System;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// FootballContext class.
    /// </summary>
    public class FootballContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FootballContext"/> class.
        /// </summary>
        public FootballContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FootballContext"/> class.
        /// </summary>
        /// <param name="options">Constructor.</param>
        public FootballContext(DbContextOptions<FootballContext> options)
        : base(options)
        {
        }

        /// <summary>
        /// Gets or Sets Players table.
        /// </summary>
        public virtual DbSet<Player> Players { get; set; }

        /// <summary>
        /// Gets or Sets Contracts table.
        /// </summary>
        public virtual DbSet<Contract> Contracts { get; set; }

        /// <summary>
        /// Gets or Sets Clubs table.
        /// </summary>
        public virtual DbSet<Club> Clubs { get; set; }

        /// <summary>
        /// Onconfiguring method.
        /// </summary>
        /// <param name="optionsBuilder">DbContextOptionsBuilder optionsBuilder.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null && !optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseLazyLoadingProxies()
                    .UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\FootballDatabase.mdf;Integrated Security=True");
            }
        }

        /// <summary>
        /// OnModelCreating method.
        /// </summary>
        /// <param name="modelBuilder">ModelBuilder modelBuilder.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder == null)
            {
                throw new ArgumentException("Modelbuilder is null");
            }

            Player p1 = new Player() { PlayerId = 1, Name = "Lionel Messi", Age = 33, Goals = 706, Nation = "Argentina", Position = "RightWinger" };
            Player p2 = new Player() { PlayerId = 2, Name = "Cristiano Ronaldo", Age = 35, Goals = 742, Nation = "Portugal", Position = "LeftWinger" };
            Player p3 = new Player() { PlayerId = 3, Name = "Szoboszlai Dominik", Age = 19, Goals = 39, Nation = "Hungary", Position = "LeftMidfield" };
            Player p4 = new Player() { PlayerId = 4, Name = "Luis Suárez", Age = 33, Goals = 467, Nation = "Uruguay", Position = "CentreForward" };
            Player p5 = new Player() { PlayerId = 5, Name = "Robert Lewandowski", Age = 32, Goals = 477, Nation = "Poland", Position = "CentreForward" };
            Player p6 = new Player() { PlayerId = 6, Name = "Andrés Iniesta", Age = 36, Goals = 84, Nation = "Spain", Position = "CentralMidfield" };
            Player p7 = new Player() { PlayerId = 7, Name = "Marcelo", Age = 32, Goals = 47, Nation = "Brazil", Position = "LeftBack" };
            Player p8 = new Player() { PlayerId = 8, Name = "Virgil van Dijk", Age = 29, Goals = 47, Nation = "Netherlands", Position = "CentreBack" };
            Player p9 = new Player() { PlayerId = 9, Name = "Neymar Junior", Age = 28, Goals = 311, Nation = "Brazil", Position = "LeftWinger" };
            Player p10 = new Player() { PlayerId = 10, Name = "Zlatan Ibrahimović", Age = 39, Goals = 532, Nation = "Sweeden", Position = "CentreForward" };
            Player p11 = new Player() { PlayerId = 11, Name = "David Neres", Age = 23, Goals = 39, Nation = "Brazil", Position = "RightWinger" };

            Club c1 = new Club() { ClubId = 1, Name = "FC Barcelona", Coach = "Ronald Koeman", Founded = 1899, Country = "Spain", ChampionshipWins = 26 };
            Club c2 = new Club() { ClubId = 2, Name = "Juventus FC", Coach = "Andrea Pirlo", Founded = 1897, Country = "Italy", ChampionshipWins = 36 };
            Club c3 = new Club() { ClubId = 3, Name = "Red Bull Salzburg", Coach = "Jesse Marsch", Founded = 1933, Country = "Spain", ChampionshipWins = 14 };
            Club c4 = new Club() { ClubId = 4, Name = "Atlético Madrid", Coach = "Diego Simeone", Founded = 1903, Country = "Spain", ChampionshipWins = 10 };
            Club c5 = new Club() { ClubId = 5, Name = "Bayern Münich", Coach = "Hans-Dieter Flick", Founded = 1900, Country = "Germany", ChampionshipWins = 30 };
            Club c6 = new Club() { ClubId = 6, Name = "Vissel Kobe", Coach = "Atsuhiro Miura", Founded = 1965, Country = "Japan", ChampionshipWins = 5 };
            Club c7 = new Club() { ClubId = 7, Name = "Real Madrid", Coach = "Zinédine Zidane", Founded = 1902, Country = "Spain", ChampionshipWins = 34 };
            Club c8 = new Club() { ClubId = 8, Name = "Liverpool FC", Coach = "Jürgen Klopp", Founded = 1892, Country = "England", ChampionshipWins = 19 };
            Club c9 = new Club() { ClubId = 9, Name = "Paris Saint-Germain", Coach = "Thomas Tuchel", Founded = 1970, Country = "France", ChampionshipWins = 9 };
            Club c10 = new Club() { ClubId = 10, Name = "AC Milan", Coach = "Stefano Pioli", Founded = 1899, Country = "Italy", ChampionshipWins = 18 };
            Club c11 = new Club() { ClubId = 11, Name = "Ajax Amsterdam", Coach = "Erik ten Hag", Founded = 1900, Country = "Netherlands", ChampionshipWins = 34 };

            Contract ct1 = new Contract() { PlayerId = 1,  ClubId = 1, ContractYears = 1, ContractDate = "2018.09.30", Salary = 45000000, ReleaseClause = 700000000, Agent = "Jorge Mendes" };
            Contract ct2 = new Contract() { PlayerId = 2,  ClubId = 2, ContractYears = 3, ContractDate = "2017.11.13", Salary = 40000000, ReleaseClause = 500000000, Agent = "Jorge Mendes" };
            Contract ct3 = new Contract() { PlayerId = 3,  ClubId = 3, ContractYears = 3, ContractDate = "2019.09.25", Salary = 2000000, ReleaseClause = 20000000, Agent = "Mino Raiola" };
            Contract ct4 = new Contract() { PlayerId = 4,  ClubId = 4, ContractYears = 1, ContractDate = "2020.09.31", Salary = 23200000, ReleaseClause = 200000000, Agent = "Jonathan Barnett" };
            Contract ct5 = new Contract() { PlayerId = 5,  ClubId = 5, ContractYears = 4, ContractDate = "2019.11.24", Salary = 27600000, ReleaseClause = 400000000, Agent = "Juan Figer" };
            Contract ct6 = new Contract() { PlayerId = 6,  ClubId = 6, ContractYears = 1, ContractDate = "2018.09.30", Salary = 25000000, ReleaseClause = 100000000, Agent = "Pep Guardiola" };
            Contract ct7 = new Contract() { PlayerId = 7,  ClubId = 7, ContractYears = 1, ContractDate = "2018.09.30", Salary = 6750000, ReleaseClause = 80000000, Agent = "Juan Figer" };
            Contract ct8 = new Contract() { PlayerId = 8,  ClubId = 8, ContractYears = 1, ContractDate = "2018.09.30", Salary = 11460000, ReleaseClause = 140000000, Agent = "Jonathan Barnett" };
            Contract ct9 = new Contract() { PlayerId = 9,  ClubId = 9, ContractYears = 1, ContractDate = "2018.09.30", Salary = 3580000, ReleaseClause = 400000000, Agent = "Jorge Mendes" };
            Contract ct10 = new Contract() { PlayerId = 10,  ClubId = 10, ContractYears = 1, ContractDate = "2018.09.30", Salary = 45000000, ReleaseClause = 150000000, Agent = "Mino Raiola" };
            Contract ct11 = new Contract() { PlayerId = 11,  ClubId = 11, ContractYears = 1, ContractDate = "2018.09.30", Salary = 45000000, ReleaseClause = 38000000, Agent = "Jorge Mendes" };

            ct1.PlayerId = p1.PlayerId;
            ct2.PlayerId = p2.PlayerId;
            ct3.PlayerId = p3.PlayerId;
            ct4.PlayerId = p4.PlayerId;
            ct5.PlayerId = p5.PlayerId;
            ct6.PlayerId = p6.PlayerId;
            ct7.PlayerId = p7.PlayerId;
            ct8.PlayerId = p8.PlayerId;
            ct9.PlayerId = p9.PlayerId;
            ct10.PlayerId = p10.PlayerId;
            ct11.PlayerId = p11.PlayerId;

            ct1.ClubId = c1.ClubId;
            ct2.ClubId = c2.ClubId;
            ct3.ClubId = c3.ClubId;
            ct4.ClubId = c4.ClubId;
            ct5.ClubId = c5.ClubId;
            ct6.ClubId = c6.ClubId;
            ct7.ClubId = c7.ClubId;
            ct8.ClubId = c8.ClubId;
            ct9.ClubId = c9.ClubId;
            ct10.ClubId = c10.ClubId;
            ct11.ClubId = c11.ClubId;

            modelBuilder.Entity<Contract>()
                .HasKey(pc => new { pc.PlayerId, pc.ClubId });
            modelBuilder.Entity<Contract>()
                .HasOne(pc => pc.Player)
                .WithMany(p => p.Contracts)
                .HasForeignKey(pc => pc.PlayerId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Contract>()
                .HasOne(pc => pc.Club)
                .WithMany(c => c.Contracts)
                .HasForeignKey(pc => pc.ClubId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Player>().HasData(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11);
            modelBuilder.Entity<Club>().HasData(c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11);
            modelBuilder.Entity<Contract>().HasData(ct1, ct2, ct3, ct4, ct5, ct6, ct7, ct8, ct9, ct10, ct11);
        }
    }
}
